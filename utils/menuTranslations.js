export default (label) => {
  switch (label) {
    case "Posts":
      return "Posztok";
    case "External-news":
      return "Külső hírek";
    case "Magazines":
      return "Hetilapok";
    case "Pages":
      return "Oldalak";
    case "Polls":
      return "Szavazások";
    case "PostStatuses":
      return "Poszt státuszok";
    case "Tags":
      return "Címkék";
    case "Topics":
      return "Rovatok";
    case "Users":
      return "Szerzők";
    case "Front page editor":
      return "Címlap beállítások";
    case "PublishInfo":
      return "Exportálási infó";
  }
  return label;
};
