"use strict";
const Parser = require("rss-parser");
const moment = require("moment");
const {
  addPost: addPostToFrontpageEditor,
  copyPostsToLive,
} = require("../../plugins/frontpage-posts/controllers/frontpage-posts");
const {
  trigger: triggerTheExport,
} = require("../../plugins/static-export/controllers/static-export");
const axios = require("axios");
const { generateAndSaveSimilars } = require("../../utils/similarArticles");

const slugify = require("slugify");

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [SECOND (optional)] [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK]
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/concepts/configurations.html#cron-tasks
 */

module.exports = {
  // https://github.com/node-cron/node-cron
  "*/10 * * * *": async () => {
    try {
      parseExternals();
    } catch (err) {
      console.log("PARSE EXTERNAL ERROR", err);
    }
  },

  "0 3 * * *": async () => {
    console.log("GENERATE SIMILARS");
    try {
      await generateSimilars();
    } catch (err) {
      console.log("GENERATE SIMILARS ERROR", err);
    }
  },

  "*/5 * * * *": async () => {
    let triggerExp = false;
    try {
      triggerExp = await checkScheduledPosts();
    } catch (err) {
      console.log("CHECK SCHEDULED POSTS", err);
    }
    try {
      triggerExp = (await checkExpiredFrontpagePosts()) || triggerExp;
    } catch (err) {
      console.log("CHECK EXPIRED FRONTPAGE POSTS", err);
    }
    console.log("TRIGGER EXPORT: ", triggerExp);

    if (triggerExp) {
      console.log("EXPORT TRIGGERED");
      let success = false;

      // tries to export 10 times, with 60sec 'sleep'. If one completes successfully breaks the loop.
      for await (const _ of Array(10)) {
        const trigger = await triggerTheExport();
        if (trigger && trigger.success) {
          success = true;
          break;
        }
        console.log("RETRYING EXPORT, reason:", trigger);
        await sleep(60000);
      }
      console.log("EXPORT ENDED SUCCESS: ", success);
    }
  },
};

async function generateSimilars() {
  console.log("GENERATING SIMILARS STARTED");
  const a = Date.now();
  const featuredTagPostsMap = {};
  const topicMap = {};

  const allPosts = (
    await strapi.connections.default
      .raw(
        "select id,topic,similarPosts,featuredTag from posts where published_at is not null"
      )
      .then()
  )[0];

  for (const post of allPosts) {
    await generateAndSaveSimilars(post, featuredTagPostsMap, topicMap);
  }

  console.log("GENERATING ENDED ", (Date.now() - a) / 1000, " s elapsed");
}

const saveLatestYTVideo = async (channelId, source) => {
  try {
    const result = await axios({
      method: "get",
      url: `https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=${channelId}&maxResults=1&order=date&type=video&key=${process.env.YOUTUBE_DATA_API_KEY}`,
    });

    if (result.status == 200) {
      const video = result.data;
      if (video && video.items && video.items.length) {
        const first = video.items[0];
        const id = first.id.videoId;

        const externalNews = await strapi.query("external-news").findOne({
          external_uuid: id,
        });

        if (!externalNews) {
          const videoDetails = await axios({
            method: "get",
            url: `https://www.googleapis.com/youtube/v3/videos?part=snippet&id=${id}&key=${process.env.YOUTUBE_DATA_API_KEY}`,
          });
          const description =
            videoDetails.data &&
            videoDetails.data.items &&
            videoDetails.data.items[0] &&
            videoDetails.data.items[0].snippet &&
            videoDetails.data.items[0].snippet.description
              ? videoDetails.data.items[0].snippet.description
              : null;

          const dateString = moment(first.snippet.publishedAt)
            .utcOffset(0)
            .format("yyyy-MM-DD HH:mm:ss");

          await strapi.query("external-news").create({
            external_uuid: id,
            title: first.snippet.title,
            text: description || first.snippet.description,
            created_at: dateString,
            published_at: dateString,
            url: `https://www.youtube.com/watch?v=${id}`,
            sourceUrl: `https://www.youtube.com/watch?v=${id}`,
            source,
            featured_image: first.snippet.thumbnails.high
              ? first.snippet.thumbnails.high.url
              : first.snippet.thumbnails.high.default,
          });
        }
      }
    }
  } catch (err) {
    console.log(err);
  }
};

const parseExternals = async () => {
  console.log("PARSE EXTERNAL SOURCES STARTED");
  const a = Date.now();

  // get youtube videos if API key is exists
  if (process.env.YOUTUBE_DATA_API_KEY) {
    /*if (process.env.FMC_YT_CHANNEL_ID) {
      await saveLatestYTVideo(process.env.FMC_YT_CHANNEL_ID, "fmc_yt");
    }*/
    if (process.env.VOROSMARTY_RADIO_YT_CHANNEL_ID) {
      await saveLatestYTVideo(
        process.env.VOROSMARTY_RADIO_YT_CHANNEL_ID,
        "vorosmarty_yt"
      );
    }
  }

  const parser = new Parser({
    customFields: {
      item: ["id"],
    },
  });

  const timeLimit = moment().subtract(180, "minutes");

  let tvFeed = await parser.parseURL(
    "https://fehervartv.ne.hu//_generated/rss/video_hu.xml"
  );
  let radioFeed = await parser.parseURL(
    "http://vorosmartyradio.hu/rss/rss_vrnews.xml"
  );

  tvFeed = tvFeed.items.map((item) => {
    const pubDate = moment(item.pubDate, [
      "ddd, DD MMM YYYY HH:mm:ss ZZ",
      "ddd, DD MMM YY HH:mm:ss ZZ",
    ]).utcOffset(0);
    const dateString = pubDate.format("yyyy-MM-DD HH:mm:ss");

    return pubDate.isAfter(timeLimit)
      ? {
          title: item.title,
          text: item.content,
          created_at: dateString,
          published_at: dateString,
          sourceUrl: item.link,
          source: "tv",
          featured_image: `https://fehervartv.ne.hu//_user/videolibrary/video_img/${item.link.replace(
            /.*&vid=(\d+)&.*/,
            (match, imageId) => imageId
          )}.jpg`,
          external_id: item.id,
        }
      : {};
  });

  radioFeed = radioFeed.items.map((item) => {
    const pubDate = moment(item.pubDate, [
      "ddd, DD MMM YYYY HH:mm:ss ZZ",
      "ddd, DD MMM YY HH:mm:ss ZZ",
    ]).utcOffset(0);
    const dateString = pubDate.format("yyyy-MM-DD HH:mm:ss");

    return pubDate.isAfter(timeLimit)
      ? {
          title: item.title,
          text: item["content:encoded"],
          created_at: dateString,
          published_at: dateString,
          sourceUrl: item.link,
          source: "radio",
          featured_image: "upload/vorosmarty_radio_logo.jpg",
          external_id: item.guid,
        }
      : {};
  });

  createFeed(tvFeed);
  createFeed(radioFeed);

  const concurrencySafetyCheck = [];
  let count = 0;

  async function createFeed(feed) {
    for (const item of feed) {
      if (!item.external_id) continue;

      const externalNews = await strapi.query("external-news").findOne({
        external_id: item.external_id,
      });

      if (
        !externalNews ||
        (externalNews.external_id != item.external_id &&
          !concurrencySafetyCheck.includes(externalNews.external_id) &&
          externalNews.title != item.title)
      ) {
        concurrencySafetyCheck.push(item.external_id);
        await strapi.query("external-news").create(item);
        count++;
      }
    }
  }

  console.log(
    `PARSE EXTERNAL SOURCES ENDED ${
      (Date.now() - a) / 1000
    }s elapsed. ${count} pcs new external article added.`
  );
};

const checkScheduledPosts = async () => {
  console.log("CHECK SCHEDULED POSTS");
  const a = Date.now();

  let shouldExport = false;

  const publishInfo = await strapi.query("publish-info").findOne({ id: 1 });
  if (publishInfo && publishInfo.status === 2) {
    console.log("HALTED POST SCHEDULE: the export is in progress");
    return shouldExport;
  }

  const now = new Date();

  const knex = strapi.connections.default;
  const Post = () => knex("posts");
  const posts = await Post()
    .select(["id", "title"])
    .where({
      published_at: null,
    })
    .where("publishDate", "<", now);

  if (posts.length) {
    for (const post of posts) {
      console.log(`PUBLISHING: [id: ${post.id}] ${post.title}`);

      await Post()
        .where({
          id: post.id,
        })
        .update({
          published_at: now,
          firstPublishedAt: now,
          modified_at: null,
          slug: slugify(post.title, { lower: true, strict: true }),
        });

      await addPostToFrontpageEditor({ request: { body: { id: post.id } } });
      shouldExport = true;
    }
    await copyPostsToLive();
  }

  console.log(
    `CHECK SCHEDULED POSTS ENDED ${(Date.now() - a) / 1000}s elapsed.`
  );
  return shouldExport;
};

const checkExpiredFrontpagePosts = async () => {
  let shouldExport = false;

  console.log("CHECK EXPIRED FRONTPAGE POSTS");
  const a = Date.now();

  const publishInfo = await strapi.query("publish-info").findOne({ id: 1 });
  if (publishInfo && publishInfo.status === 2) {
    console.log("HALTED EXPIRED FRONTPAGE POSTS: the export is in progress");
    return shouldExport;
  }

  const now = new Date();

  const knex = strapi.connections.default;
  const Post = () => knex("posts");

  const FrontpageEditor = () => knex("front_page_editors");

  const { liveFrontpagePosts: fpData } = await FrontpageEditor()
    .select("liveFrontpagePosts")
    .first();

  // ha lockolva van az array megadjuk a poziciojat
  const frontpagePosts = JSON.parse(fpData).map((val, i) => {
    return {
      ...val,
      ...(val.isLocked && { lockedPosition: i }),
    };
  });

  const MAX_REQ_POST = frontpagePosts.length;

  const temp = frontpagePosts.filter((post) => {
    return !(
      post.removalDate &&
      moment(post.removalDate).diff(moment(now), "minutes") < 3
    );
  });

  // ha nem MAX_REQ_POST elemu akkor fel kell tolteni remove szamu elemmel
  const removed = MAX_REQ_POST - temp.length;

  if (removed > 0) {
    const result = await Post()
      .limit(MAX_REQ_POST)
      .orderBy("published_at", "desc");

    // Published alapjan lekert cikkekbol kiszedjuk azokat amik a tempbe mar benne vannak
    const filteredResult = result.filter(
      (post) => !temp.some((p) => p.postId === post.id)
    );

    // osszeallitjuk a feltoltendo cikkekkel
    const fills = Array(removed)
      .fill()
      .map((_, i) => {
        return {
          postId: filteredResult[i].id,
          isLocked: false,
        };
      });

    let newPostsWithoutLocks = [...temp, ...fills];
    let newPosts = [...newPostsWithoutLocks];

    // itt a helyere tesszuk az elmozdult cikkeket
    for (const [i, post] of newPostsWithoutLocks.entries()) {
      if (post.lockedPosition) {
        newPosts = moveElement(newPosts, i, post.lockedPosition);
      }
    }

    // kiszedjuk a lockolt pozicio indexet
    const newPostData = newPosts.map((post) => {
      delete post.lockedPosition;
      return post;
    });

    await FrontpageEditor()
      .where({
        id: 1,
      })
      .update({
        liveFrontpagePosts: JSON.stringify(newPostData),
        frontpagePosts: JSON.stringify(newPostData),
      });
    shouldExport = true;
  }

  console.log(
    `CHECK EXPIRED FRONTPAGE POSTS ENDED ${(Date.now() - a) / 1000}s elapsed.`
  );
  return shouldExport;
};

const moveElement = (array, from, to) => {
  const copy = [...array];
  const valueToMove = copy.splice(from, 1)[0];
  copy.splice(to, 0, valueToMove);
  return copy;
};

const sleep = async (ms) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
};
