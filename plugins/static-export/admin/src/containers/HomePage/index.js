/*
 *
 * HomePage
 *
 */

import React, { memo } from "react";
// import PropTypes from 'prop-types';
import pluginId from "../../pluginId";
import StaticExportButton from "../../components/StaticExportButton";

const HomePage = () => {
  return (
    <div className="container-fluid" style={{ padding: "25px 20px" }}>
      <div className="row">
        <div className="col-12">
          <h1>Static Export Button</h1>
          <p style={{ fontSize: 18 }}>
            If you want to publish your contents, you need to export.
          </p>
          <p style={{ fontSize: 16 }}>
            Press the button below to start the export:
          </p>
          <StaticExportButton />
        </div>
      </div>
    </div>
  );
};

export default memo(HomePage);
