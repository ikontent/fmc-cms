import React, { useRef } from "react";
import { useClickOutside } from "../../utils/useClickoutside";

export const ClickoutsideWrapper = ({ children, handler }) => {
  const wrapperRef = useRef(null);
  useClickOutside(wrapperRef, handler);
  return <div ref={wrapperRef}>{children}</div>;
};
