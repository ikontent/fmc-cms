"use strict";
const axios = require("axios");

const { exec } = require("child_process");
const { appendFileSync } = require("fs");

/**
 * static-export.js controller
 *
 * @description: A set of functions called "actions" of the `static-export` plugin.
 */

module.exports = {
  /**
   * Default action.
   *
   * @return {Object}
   */

  index: async (ctx) => {
    // Add your own logic here.

    // Send 200 `ok`
    ctx.send({
      message: "pinged",
    });
  },

  check: async (ctx) => {
    // Check Bitbucket Pipeline building status.
    try {
      let publishInfo = await strapi.query("publish-info").findOne({ id: 1 });
      if (!publishInfo) {
        publishInfo = await strapi
          .query("publish-info")
          .create({ status: 0, lastPublished: null });
      }

      const pipelineState = {
        isCompleted: false,
        success: null,
      };

      switch (publishInfo.status) {
        case 0:
          pipelineState.isCompleted = true;
          pipelineState.success = false;
          break;
        case 1:
          pipelineState.isCompleted = true;
          pipelineState.success = true;
          pipelineState.lastSuccess = publishInfo.lastPublished;
          break;
        case 2:
          //its okay
          break;
      }
      return { pipelineState };
    } catch (error) {
      console.log("ERROR IN PIPELINE CHECK");
      console.log(error);

      ctx.response.status = 500;
      return ctx.send(
        JSON.stringify({
          error: `Internal error, check api logs`,
        })
      );
    }
  },

  trigger: async (ctx) => {
    // Trigger Bitbucket Pipeline to execute build process.
    try {
      const publishInfo = await strapi.query("publish-info").findOne({ id: 1 });

      if (publishInfo.status === 2) {
        if (ctx) {
          ctx.response.status = 200;
        }
        return "Publikálás folyamatban!";
      }

      await strapi.query("publish-info").update({ id: 1 }, { status: 2 });
      exec(
        "./publish.sh",
        {
          cwd: process.env.WEB_PATH,
          ...(process.env.UID ? { uid: parseInt(process.env.UID) } : {}),
          ...(process.env.GID ? { gid: parseInt(process.env.GID) } : {}),
        },
        (error, stdout, stderr) => {
          if (error) {
            console.error(`exec error: ${error}`);
            appendFileSync(
              `${process.env.WEB_PATH}/publish.log`,
              `ERROR ÁG: ${error}`
            );
            strapi.query("publish-info").update({ id: 1 }, { status: 0 });
            return;
          }

          strapi
            .query("publish-info")
            .update({ id: 1 }, { status: 1, lastPublished: new Date() });

          console.log(`stdout: ${stdout}`);
          console.error(`stderr: ${stderr}`);
          appendFileSync(
            `${process.env.WEB_PATH}/publish.log`,
            `stdout: ${stdout}\nstderr: ${stderr}`
          );
        }
      );

      return { success: true };
    } catch (err) {
      console.log("ERROR IN PIPELINE TRIGGER");
      console.log(err);
      strapi.query("publish-info").update({ id: 1 }, { status: 0 });

      if (ctx) {
        ctx.response.status = 500;
      }
      return {
        error: `Internal error, check api logs`,
      };
    }
  },
};
