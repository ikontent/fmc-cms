import React, {
  useEffect,
  useImperativeHandle,
  useState,
  forwardRef,
} from "react";
import { unstable_batchedUpdates } from "react-dom";
import AsyncSelect from "react-select/async";
import { request } from "strapi-helper-plugin";

export const SponsoredContent = forwardRef((_, ref) => {
  const [sidebarSponsoredTag, setSidebarSponsoredTag] = useState(null);

  useImperativeHandle(ref, () => ({
    async saveSaveSponsoredContent() {
      const { result } = await request(
        `${strapi.backendURL}/frontpage-posts/save-sponsored-posts`,
        {
          method: "POST",
          body: {
            postId: sidebarSponsoredTag ? sidebarSponsoredTag.value : undefined,
          },
        }
      );
      if (result === "ok") {
        return "ok";
      } else {
        strapi.notification.error(
          "Hiba történt a szponzorált cikk mentésekor!"
        );
      }
    },
  }));

  useEffect(() => {
    (async () => {
      try {
        const { result } = await request(
          `${strapi.backendURL}/frontpage-posts/sidebar-sponsored-post`
        );
        unstable_batchedUpdates(() => {
          const { post } = result;
          setSidebarSponsoredTag(
            post ? { label: post.title, value: post.id } : null
          );
        });
      } catch (err) {
        console.log(err);
        strapi.notification.error("Something happened during loading posts!");
      }
    })();
  }, []);

  const promiseOptions = async (inputValue) => {
    const resp = await request(
      `${strapi.backendURL}/posts?title_contains=${inputValue}&_sort=published_at:DESC&_limit=10`
    );
    return resp.map((post) => {
      return {
        label: post.title,
        value: post.id,
      };
    });
  };

  const onChange = async (selectedOptions) => {
    setSidebarSponsoredTag(selectedOptions);
  };

  return (
    <div
      style={{
        margin: "4rem 0",
        position: "relative",
        zIndex: 99,
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <h2>Kiemelt szponzorált cikk</h2>
        <div
          style={{
            marginLeft: "2rem",
            width: "600px",
          }}
        >
          <AsyncSelect
            defaultOptions
            value={sidebarSponsoredTag}
            loadOptions={promiseOptions}
            onChange={onChange}
            isClearable={true}
          />
        </div>
      </div>
    </div>
  );
});
