"use strict";
const slugify = require("slugify");
const Boom = require("boom");
const {
  addPost,
  hasFrontpageThisPost,
} = require("../../../plugins/frontpage-posts/controllers/frontpage-posts");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async beforeCreate(data) {
      data.videoURL = data.content
        ? data.content.indexOf("youtube.com/embed") != -1
        : false;

      if (!data.status) {
        // default status is: "Vázlat"
        data.status = 4;
      }

      validateFields(data);

      await preventTopicTagMatch(data);
    },

    async afterCreate(result, data) {},

    async beforeUpdate(params, data) {
      if (isSaving(data)) {
        // check to see if the article has been modified and change modified_at
        try {
          const post = await strapi.query("post").findOne({ id: params.id });

          if (
            post.lead != data.lead ||
            post.content != data.content ||
            post.title != data.title
          ) {
            data.modified_at = new Date();
          }

          data.published_at = post.published_at;
        } catch (e) {
          console.log(e);
        }

        if (data.published_at && !data.featuredImage) {
          throw Boom.boomify(
            Error("Nem törölhető a kép, mert publikált a cikk"),
            { statusCode: 422 }
          );
        }

        if (data.content) {
          data.videoURL = data.content
            ? data.content.indexOf("youtube.com/embed") != -1
            : false;
        }

        if (!data.status) {
          // default status is: "Vázlat"
          data.status = 4;
        }

        if (data.title !== undefined) {
          validateFields(data);
        }

        await preventTopicTagMatch(data);
      }

      if (isPublishing(data)) {
        // when a post is publishing we set the state to "Publikálható"
        data.status = 2;

        // generate slug on first time publish
        try {
          const post = await strapi.query("post").findOne({ id: params.id });

          if (post.title && !post.slug) {
            data.slug = slugify(post.title, { lower: true, strict: true });
          }
          if (!post.firstPublishedAt) {
            data.firstPublishedAt = new Date();
          }

          data.featuredImage = post.featuredImage;
        } catch (e) {
          console.log(e);
        }

        validateField(
          data,
          "featuredImage",
          "A Kiemelt kép kötelező a publikáláshoz"
        );
      }

      // Exported live post in frontpage can not be unpublished
      if (isUnpublishing(data)) {
        if (await hasFrontpageThisPost({ params: { id: params.id } })) {
          const err = new Error(
            "A cikket nem lehet vázlattá alakítani mert a címlapra ki van rakva. Előbb vegye le a címlapról a cikket."
          );
          const boomError = Boom.boomify(err, {
            statusCode: 422,
          });
          throw boomError;
        }
      }
    },
  },
};

const isSaving = (data) => {
  return Object.keys(data).length > 2;
};

const isPublishing = (data) => {
  return !!(data.published_at && Object.keys(data).length === 1);
};

const isUnpublishing = (data) => {
  return data.published_at === null && Object.keys(data).length === 1;
};

const validateFields = (data) => {
  validateField(data, "title", "A cím megadása kötelező");
  validateField(data, "lead", "A lead megadása kötelező");
  validateField(data, "content", "A tartalom megadása kötelező");
  validateField(data, "author", "A szerző kitöltése kötelező");
  validateField(data, "featuredTag", "A kiemelt címke megadása kötelező");
  validateField(data, "topic", "A rovat megadása kötelező");
};

const validateField = (data, field, message = "Hibás űrlap") => {
  if (!data[field]) {
    const err = new Error(message);
    const boomError = Boom.boomify(err, {
      statusCode: 422,
    });
    throw boomError;
  }
};

const preventTopicTagMatch = async (data) => {
  const { topic, tag } = await getTopicTag(data);

  if (stripAccents(topic) === stripAccents(tag)) {
    throw Boom.boomify(Error("A rovat és a kiemelt címke nem lehet azonos"), {
      statusCode: 422,
    });
  }
};

const getTopicTag = async (data) => {
  try {
    const { name: topic } = await strapi
      .query("topic")
      .findOne({ id: data.topic });

    const { tagName: tag } = await strapi
      .query("tag")
      .findOne({ id: data.featuredTag });

    return { topic, tag };
  } catch (e) {
    console.log(e);
  }
};

const stripAccents = (str) => {
  return str
    ? str
        .toLowerCase()
        .replace(/á/g, "a")
        .replace(/é/g, "e")
        .replace(/í/g, "i")
        .replace(/[óöő]/g, "o")
        .replace(/[úüű]/g, "u")
    : "";
};
