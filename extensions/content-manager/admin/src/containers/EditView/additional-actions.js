"use strict";

import { request } from "strapi-helper-plugin";

module.exports = (WEB_URL, PREVIEW_CLIENT_URL_KEY, PREVIEW_API_URL_KEY) => ({
  "application::post.post": {
    Előnézet: ({ id }) => {
      window.open(
        `${WEB_URL}/preview/${PREVIEW_CLIENT_URL_KEY}?previewApiKey=${PREVIEW_API_URL_KEY}&id=${id}`
      );
    },
  },
  "application::module.module": {},
  "plugins::users-permissions.user": {},
});
