: # Universal script. You can run on linux and windows.
: # How the hell this works: https://stackoverflow.com/a/17623721/4543092
: #
: # Usage:
: # dd			Stop all docker containers and run the current
: # dd up		Stop all docker containers and run the current then enter the docker bash
: # dd enter	Enter the current docker container bash
: # dd ps	    Lists containers
: # dd stop		Stop the current docker container
: # dd down		Down the current docker container



:<<"::CMDLITERAL"
@ECHO OFF
GOTO :CMDSCRIPT
::CMDLITERAL




if [ -z "$1" ]
then
	docker stop $(docker ps -a -q)
	docker-compose up -d
fi
if [[ $1 == "enter" ]]
then
	docker exec -it $(docker ps -f name=wordpress -q) bash
fi
if [[ $1 == "up" ]]
then
	docker stop $(docker ps -a -q)
	docker-compose up -d
	docker exec -it $(docker ps -f name=wordpress -q) bash
fi
if [[ $1 == "ps" ]]
then
	docker-compose ps
fi
if [[ $1 == "stop" ]]
then
	docker-compose stop
fi
if [[ $1 == "down" ]]
then
	docker-compose down
fi
exit $?






:CMDSCRIPT
@ECHO OFF
IF "%1"=="" (
	for /f "tokens=*" %%f in ('docker ps -a -q') do @(docker stop "%%f")
	docker-compose up -d
)
IF "%1"=="enter" (
	for /f "tokens=*" %%f in ('docker ps -f "name=wordpress" -q') do @(docker exec -it "%%f" bash)
)
IF "%1"=="up" (
	for /f "tokens=*" %%f in ('docker ps -a -q') do @(docker stop "%%f")
	docker-compose up -d
	for /f "tokens=*" %%f in ('docker ps -f "name=wordpress" -q') do @(docker exec -it "%%f" bash)
)
IF "%1"=="ps" (
	docker-compose ps
)
IF "%1"=="stop" (
	docker-compose stop
)
IF "%1"=="down" (
	docker-compose down
)
