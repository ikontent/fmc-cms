module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: {
        charset: "utf8mb4",
        client: "mysql",
        host: env("DATABASE_HOST", "127.0.0.1"),
        port: env.int("DATABASE_PORT", 3306),
        database: env("DATABASE_NAME", "fmc"),
        username: env("DATABASE_USERNAME", "fmc"),
        password: env("DATABASE_PASSWORD", "fmc123"),
        ssl: env.bool("DATABASE_SSL", false),
      },
      options: {
        charset: "utf8mb4_general_ci",
        pool: {
          max: 5,
        },
      },
    },
  },
});
