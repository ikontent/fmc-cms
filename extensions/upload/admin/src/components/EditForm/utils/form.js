import { getTrad } from "../../../utils";

const form = [
  {
    key: 1,
    inputs: [
      {
        label: { id: getTrad("form.input.label.file-title") },
        name: "fileInfo.title",
      },
    ],
  },
  {
    key: 2,
    inputs: [
      {
        label: { id: getTrad("form.input.label.file-keywords") },
        name: "fileInfo.keywords",
      },
    ],
  },
  {
    key: 3,
    inputs: [
      {
        label: { id: getTrad("form.input.label.file-author") },
        name: "fileInfo.author",
      },
    ],
  },
  {
    key: 4,
    inputs: [
      {
        label: { id: getTrad("form.input.label.file-description") },
        name: "fileInfo.description",
      },
    ],
  },
  {
    key: 5,
    inputs: [
      {
        label: { id: getTrad("form.input.label.file-location") },
        name: "fileInfo.location",
      },
    ],
  },
  {
    key: 6,
    inputs: [
      {
        label: { id: getTrad("form.input.label.file-event") },
        name: "fileInfo.event",
      },
    ],
  },
  {
    key: 7,
    inputs: [
      {
        description: { id: getTrad("form.input.decription.file-alt") },
        label: { id: getTrad("form.input.label.file-alt") },
        name: "fileInfo.alt",
      },
    ],
  },
];

export default form;
