import React, { useEffect, useState } from "react";

import useDataManager from "../../hooks/useDataManager";

import { ReactSortable } from "react-sortablejs";

import SelectWrapper from "../../components/SelectWrapper";

import { request } from "strapi-helper-plugin";

/*const { data } = await request(requestURL, {
        method: 'POST',
        body: {
          contentTypeUID,
          field: name,
          data: modifiedData,
        },
      });*/

const EditSimilar = () => {
  const { initialData, onChange, modifiedData } = useDataManager();

  let { similarPosts } = modifiedData;

  if (!similarPosts) {
    similarPosts = [];
  }

  const [idMapState, setIdMapState] = useState();

  useEffect(() => {
    if (similarPosts.length && !idMapState) {
      (async () => {
        const data = await request(`${strapi.backendURL}/posts/get-similar`, {
          params: { id_in: similarPosts },
        });
        if (data?.[0]?.id) {
          setIdMapState(
            data.reduce((acc, { id, title }) => {
              acc[id] = title;
              return acc;
            }, {})
          );
        }
      })();
    }
  }, [similarPosts, idMapState]);

  const isChanged = (orig, dest) => {
    return (
      !orig ||
      dest.some((id, index) => id !== orig?.[index]) ||
      orig.some((id, index) => id !== dest?.[index])
    );
  };
  const idMap = idMapState || {};
  return similarPosts && similarPosts.map ? (
    <div
      className="row"
      style={{ paddingBottom: "3rem", marginBottom: "10rem" }}
    >
      <div className="col-6">
        <h1>Hasonló cikkek</h1>

        <ReactSortable
          list={similarPosts}
          setList={() => {}}
          animation={200}
          forceFallback={true}
          onEnd={(evt) => {
            let oldI = evt.oldIndex;
            let newI = evt.newIndex;

            let newAnswers = [...similarPosts];

            let element = newAnswers[oldI];
            newAnswers.splice(oldI, 1);
            newAnswers.splice(newI, 0, element);

            onChange({
              target: {
                value: isChanged(initialData.similarPosts, newAnswers)
                  ? [...newAnswers]
                  : [...initialData.similarPosts],
                name: "similarPosts",
                type: "json",
              },
            });
          }}
        >
          {similarPosts.map((id, index) => (
            <div key={id} className="d-flex">
              <div
                style={{
                  border: "0.5px solid black",
                  borderRadius: "4px",
                  marginBottom: "4px",
                  cursor: "grabbing",
                  width: "100%",
                  padding: "4px",
                }}
                className="d-flex answer-editor"
              >
                <span>{idMap[id] || id}</span>
              </div>
              <button
                style={{ float: "right" }}
                onClick={(e) => {
                  e.stopPropagation();
                  e.preventDefault();
                  onChange({
                    target: {
                      value: similarPosts.filter((_, i) => i !== index),
                      name: "similarPosts",
                      type: "json",
                    },
                  });
                }}
              >
                ❌
              </button>
            </div>
          ))}
        </ReactSortable>
        {similarPosts?.length < 5 && (
          <div style={{ marginTop: "24px" }}>
            <SelectWrapper
              {...{
                model: "post",
                relationType: "oneWay",
                targetModel: "application::post.post",
                type: "relation",
              }}
              {...{
                description: "",
                editable: true,
                label: "Hasonló hozzáadása",
                mainField: {
                  name: "title",
                  schema: { type: "string" },
                  placeholder: "",
                  visible: true,
                },
              }}
              queryInfos={{
                containsKey: "title_contains",
                defaultParams: {
                  id_nin: [
                    initialData.id ? [initialData.id] : [],
                    ...similarPosts,
                  ],
                },
                endPoint: "/posts/get-similar",
                shouldDisplayRelationLink: true,
              }}
              key={"topic"}
              name={"topic"}
              relationsType={"oneWay"}
              forcedOnChange={(e, setInit) => {
                const { id, title } = e.target.value || {};
                if (id) {
                  similarPosts.push(id);
                  idMap[id] = title;
                  onChange({
                    target: {
                      value: isChanged(initialData.similarPosts, similarPosts)
                        ? [...similarPosts]
                        : [...initialData.similarPosts],
                      name: "similarPosts",
                      type: "json",
                    },
                  });
                  setInit();
                }
              }}
              doNotUseValue
              hacky={initialData?.id}
            />
          </div>
        )}
      </div>
    </div>
  ) : null;
};
export default EditSimilar;
