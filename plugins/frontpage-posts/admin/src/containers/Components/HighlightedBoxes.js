import React, {
  useEffect,
  useState,
  forwardRef,
  useImperativeHandle,
} from "react";
import useDebounce from "../../utils/useDebounce";
import { request } from "strapi-helper-plugin";
import moment from "moment";
import { InputText, Button } from "@buffetjs/core";
import { unstable_batchedUpdates } from "react-dom";
import AsyncSelect from "react-select/async";

export const HighlightedBox = forwardRef((_, ref) => {
  const [highlightedTags, setHighlightedTags] = useState([]);

  useImperativeHandle(ref, () => ({
    async saveHighlightedBoxes() {
      try {
        if (
          highlightedTags.some((tag) => {
            return tag.blockName.length === 0;
          })
        ) {
          strapi.notification.error("A Boxok címét meg kell adnod!");
          return false;
        }

        const { result } = await request(
          `${strapi.backendURL}/frontpage-posts/save-highlighted-box`,
          {
            method: "POST",
            body: {
              highlightedTags,
            },
          }
        );

        return result;
      } catch (err) {
        console.log(err);
        strapi.notification.error("Something happened during loading posts!");
      }
    },
  }));

  useEffect(() => {
    async function fetchData() {
      try {
        const { result } = await request(
          `${strapi.backendURL}/frontpage-posts/highlighted-box`
        );
        const highlightedBox = await Promise.all(
          result.tags.map(async (tag) => {
            const {
              result: { posts },
            } = await getMostRecentArticles(tag.tags);
            return {
              ...tag,
              posts,
            };
          })
        );
        setHighlightedTags(highlightedBox);
      } catch (err) {
        console.log(err);
        strapi.notification.error("Something happened during loading posts!");
      }
    }
    fetchData();
  }, []);

  const getMostRecentArticles = async (tags) => {
    return await request(
      `${strapi.backendURL}/frontpage-posts/highlighted-box-articles`,
      {
        method: "POST",
        body: tags.map((t) => {
          return { id: t.value };
        }),
      }
    );
  };

  const onChange = async (selectedOptions, { name: index }) => {
    if (selectedOptions && selectedOptions.length > 5) return;
    const highlightedBoxes = [...highlightedTags];
    if (!selectedOptions) {
      highlightedBoxes[index].posts = [];
      highlightedBoxes[index].tags = [];
    } else {
      const {
        result: { posts },
      } = await getMostRecentArticles(selectedOptions);
      highlightedBoxes[index].posts = posts;
      highlightedBoxes[index].tags = selectedOptions;
    }
    setHighlightedTags(highlightedBoxes);
  };

  const promiseOptions = async (inputValue) => {
    const resp = await request(
      `${strapi.backendURL}/tags?tagName_contains=${inputValue}&_limit=10`
    );
    return resp.map((tag) => {
      return {
        label: tag.tagName,
        value: tag.id,
      };
    });
  };

  return (
    <div
      style={{
        margin: "4rem 0",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <h2>Kiemelt boxok</h2>
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
        }}
      >
        {highlightedTags.length > 0 &&
          highlightedTags.map((tag, i) => (
            <div
              key={i}
              style={{
                display: "flex",
                flexDirection: "column",
                flex: 1,
                marginRight: "2rem",
              }}
            >
              <h3
                style={{
                  marginTop: "1rem",
                  marginBottom: "1rem",
                }}
              >
                Box{i + 1} címe:
              </h3>
              <InputText
                name={String(i)}
                onChange={(e) => {
                  const hTags = [...highlightedTags];
                  hTags[i].blockName = e.target.value;
                  setHighlightedTags(hTags);
                }}
                type="text"
                value={tag.blockName}
              />
              <h3
                style={{
                  marginTop: "1rem",
                  marginBottom: "1rem",
                }}
              >
                Címkék
              </h3>
              <AsyncSelect
                isMulti
                name={i}
                defaultValue={tag.tags}
                loadOptions={promiseOptions}
                onChange={onChange}
                isOptionDisabled={() => {
                  return tag.tags.length >= 5;
                }}
              />
              <ul>
                {tag.posts.length > 0 &&
                  tag.posts.map((post) => {
                    return <li key={post.title}>{post.title}</li>;
                  })}
              </ul>
            </div>
          ))}
      </div>
    </div>
  );
});
