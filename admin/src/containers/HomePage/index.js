/*
 *
 * HomePage
 *
 */
/* eslint-disable */
import React, { memo, useMemo, useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
import { get, upperFirst } from "lodash";
import {
  auth,
  LoadingIndicatorPage,
  LoadingIndicator,
  request,
} from "strapi-helper-plugin";
import PageTitle from "../../components/PageTitle";
import { useModels } from "../../hooks";
import { Link } from "react-router-dom";
import moment from "moment";

import useFetch from "./hooks";
import {
  ALink,
  Block,
  Container,
  LinkWrapper,
  P,
  Wave,
  Separator,
} from "./components";

const EditIcon = () => {
  return (
    <svg
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="pencil-alt"
      className="svg-inline--fa fa-pencil-alt fa-w-16 "
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
    >
      <path
        fill="currentColor"
        d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"
      ></path>
    </svg>
  );
};

const HomePage = ({ history: { push } }) => {
  const userInfo = auth.getUserInfo();
  const username = get(userInfo, "firstname", "");
  const userId = userInfo.id;

  const userRoles = userInfo.roles;
  const isSuperAdmin = userRoles.some((x) => x.id === 1);
  const isAuthor = userRoles.some((x) => x.id === 3);

  const [draftPosts, setDraftPosts] = useState([]);
  const [loadingDraftPosts, setLoadingDraftPosts] = useState(true);
  const [draftTags, setDraftTags] = useState([]);
  const [loadingDraftTags, setLoadingDraftTags] = useState(true);
  const {
    //collectionTypes,
    //singleTypes,
    isLoading: isLoadingForModels,
  } = useModels();

  useEffect(() => {
    (async () => {
      const posts = await request(
        `${strapi.backendURL}/posts/get-latest-drafts/${userId}/${
          isAuthor ? 1 : 0
        }`
      );
      setDraftPosts(posts);
      setLoadingDraftPosts(false);

      if (isSuperAdmin) {
        const tags = await request(
          `${strapi.backendURL}/tags/get-latest-drafts`
        );
        setDraftTags(tags);
        setLoadingDraftTags(false);
      }
    })();
  }, []);

  if (isLoadingForModels) {
    return <LoadingIndicatorPage />;
  }

  return (
    <>
      <FormattedMessage id="HomePage.helmet.title">
        {(title) => <PageTitle title={title} />}
      </FormattedMessage>
      <Container className="container-fluid">
        <div className="row">
          <div className="col-12">
            <Block>
              <Wave />
              <h2>Üdv {upperFirst(username)}!</h2>
            </Block>

            {isSuperAdmin && (
              <Block>
                <h3 className="mb-3">Címke vázlatok (Tag draftok)</h3>
                {loadingDraftTags ? (
                  <LoadingIndicator />
                ) : draftTags.length > 0 ? (
                  <div>
                    {draftTags.map((tag, i) => (
                      <Link
                        key={i}
                        to={`/plugins/content-manager/collectionType/application::tag.tag/${tag.id}`}
                        style={{
                          color: "#333740",
                          textDecoration: "none",
                        }}
                      >
                        <div className="draft-row">
                          <span>
                            {tag.tagName}{" "}
                            <span className="ml-2">
                              ({moment(tag?.created_at).format("yyyy.MM.DD")})
                            </span>
                          </span>
                          <EditIcon />
                        </div>
                      </Link>
                    ))}
                  </div>
                ) : (
                  <div>Nincsenek elfogadásra váró tag-ek. 🍕</div>
                )}
              </Block>
            )}

            <Block>
              <h3 className="mb-3">Cikk vázlatok (Post draftok)</h3>
              {loadingDraftPosts ? (
                <LoadingIndicator />
              ) : draftPosts.length > 0 ? (
                <div>
                  {draftPosts.map((post, i) => (
                    <Link
                      key={i}
                      to={`/plugins/content-manager/collectionType/application::post.post/${post.id}`}
                      style={{
                        color: "#333740",
                        textDecoration: "none",
                      }}
                    >
                      <div className="draft-row">
                        <span>
                          {post.title}{" "}
                          <span className="ml-2">
                            ({moment(post?.created_at).format("yyyy.MM.DD")})
                          </span>
                          <strong className="ml-5">{post.author}</strong>
                        </span>
                        <EditIcon />
                      </div>
                    </Link>
                  ))}
                </div>
              ) : (
                <div>Nincsenek cikk vázlatok! ☕</div>
              )}
            </Block>
          </div>
        </div>
      </Container>
      <style>{`
            .draft-row {
              background: #fff;
              color: #333740;
              margin-bottom: 1px;
              border-bottom: 1px solid #f1f1f2;
              padding: 1.3rem 2.5rem;
              cursor: pointer;
              display: flex;
              justify-content: space-between;
            }
            .draft-row:hover {
              background: #f7f8f8;
            }
          `}</style>
    </>
  );
};

export default memo(HomePage);
