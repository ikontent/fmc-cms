"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  answer: async ({ request, params }) => {
    const { id } = params;
    const { answerArray } = request.body;
    const poll = await strapi.query("poll").findOne({
      id,
    });

    answerArray.forEach((element) => {
      poll.answers[element].count++;
    });

    await strapi.query("poll").update({ id }, { answers: poll.answers });

    return { success: true };
  },
};
