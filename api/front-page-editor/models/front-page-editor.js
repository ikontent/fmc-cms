"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async beforeCreate(data) {
      if (data.breakingNews && data.breakingNews.expireAt == null) {
        data.breakingNews.expireAt = new Date(Date.now() + 3600 * 1000 * 24);
      }
    },
    async beforeUpdate(params, data) {
      if (data.breakingNews && data.breakingNews.expireAt == null) {
        data.breakingNews.expireAt = new Date(Date.now() + 3600 * 1000 * 24);
      }
    },
  },
};
