module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port: env.int("PORT", 1337),
  cron: { enabled: true },
  admin: {
    auth: {
      secret: env("ADMIN_JWT_SECRET", "90db620326da16e1988d2302e4a5ab82"),
    },
  },
});
