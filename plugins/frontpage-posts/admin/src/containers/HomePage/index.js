/*
 *
 * HomePage
 *
 */

import React, { memo } from "react";
// import PropTypes from 'prop-types';
import pluginId from "../../pluginId";
import FrontpagePostEditor from "../Components/FrontpagePostEditor";

const HomePage = () => {
  return (
    <div style={{ padding: "2rem" }}>
      <FrontpagePostEditor />
    </div>
  );
};

export default memo(HomePage);
