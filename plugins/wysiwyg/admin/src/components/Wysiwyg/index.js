import React, { useState } from "react";
import PropTypes from "prop-types";
import { isEmpty } from "lodash";
import { Button } from "@buffetjs/core";
import { Label, InputDescription, InputErrors } from "strapi-helper-plugin";
import Editor from "../Tinymce";
import MediaLib from "../MediaLib";

const Wysiwyg = ({
  inputDescription,
  errors,
  label,
  name,
  noErrorsDescription,
  onChange,
  value,
}) => {
  const [editorInstance, setEditorInstance] = useState(null);

  const [isOpen, setIsOpen] = useState(false);
  let spacer = !isEmpty(inputDescription) ? (
    <div style={{ height: ".4rem" }} />
  ) : (
    <div />
  );

  if (!noErrorsDescription && !isEmpty(errors)) {
    spacer = <div />;
  }

  // media selected
  const handleMediaLibChange = (data) => {
    if (data.mime.includes("image")) {
      const imgTag = `<p><img src="${data.url}" caption="${data.caption}" alt="${data.alternativeText}"></img></p>`;
      editorInstance.insertContent(imgTag);
    }
  };

  // get tinymce editor instance
  const getInstance = (instance) => {
    setEditorInstance(instance);
  };

  const handleToggle = () => setIsOpen((prev) => !prev);

  return (
    <div
      style={{
        marginBottom: "1.6rem",
        fontSize: "1.3rem",
        fontFamily: "Lato",
      }}
    >
      <Label htmlFor={name} message={label} style={{ marginBottom: 10 }} />
      <div style={{ marginBottom: "0.5rem" }}>
        <Button
          color="primary"
          onClick={handleToggle}
          style={{ display: "none" }}
        >
          + Add picture
        </Button>
        <MediaLib
          onToggle={handleToggle}
          isOpen={isOpen}
          onChange={handleMediaLibChange}
          name={name}
        />
      </div>
      {name == "lead" ? (
        <>
          <textarea
            name={name}
            onChange={onChange}
            value={(value || "").replace(/<\/?[^>]+(>|$)/g, "")}
            style={{
              border: "1px solid #E3E9F3",
              borderRadius: "2px",
              width: "100%",
              minHeight: "80px",
              padding: "1rem",
              backgroundColor: value?.length > 300 ? "#FF3311" : "#fff",
            }}
          ></textarea>
          {value?.length || "0"}
        </>
      ) : (
        <Editor
          name={name}
          onChange={onChange}
          value={value || ""}
          onInstance={getInstance}
        />
      )}
      <InputDescription
        message={inputDescription}
        style={!isEmpty(inputDescription) ? { marginTop: "1.4rem" } : {}}
      />
      <InputErrors
        errors={(!noErrorsDescription && errors) || []}
        name={name}
      />
    </div>
  );
};

Wysiwyg.defaultProps = {
  errors: [],
  inputDescription: null,
  label: "",
  noErrorsDescription: false,
  value: "",
};

Wysiwyg.propTypes = {
  errors: PropTypes.array,
  inputDescription: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
    PropTypes.shape({
      id: PropTypes.string,
      params: PropTypes.object,
    }),
  ]),
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
    PropTypes.shape({
      id: PropTypes.string,
      params: PropTypes.object,
    }),
  ]),
  name: PropTypes.string.isRequired,
  noErrorsDescription: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

export default Wysiwyg;
