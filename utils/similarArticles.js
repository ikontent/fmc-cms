async function generateAndSaveSimilars(
  post,
  featuredTagPostsMap = {},
  topicMap = {}
) {
  if (!post) {
    return;
  }

  const { id, topic, featuredTag, similarPosts } = post;

  let featured100 = [],
    topic500 = [];

  if (featuredTag && featuredTag !== 169) {
    if (featuredTagPostsMap[featuredTag]) {
      featured100 = featuredTagPostsMap[featuredTag];
    } else {
      featured100 = (
        await strapi.connections.default
          // .raw(
          //   `select id from posts where id <> ${id} and published_at is not null and featuredTag = ${featuredTag} and topic <> 42 and topic <> 27  and topic <> 7 order by created_at desc limit 100`
          // )

          // posts with tag "friss" should also be excluded
          .raw(
            `select p.id from posts p left join posts_tags__tags_posts pt on p.id = pt.post_id and pt.tag_id <> 169 where p.id <> ${id} and p.published_at is not null and p.featuredTag = ${featuredTag} and p.topic <> 42 and p.topic <> 27 and p.topic <> 7 order by p.created_at desc limit 100`
          )
          .then()
      )[0];
      featuredTagPostsMap[featuredTag] = featured100;
    }
  }

  if (topic && topic !== 42 && topic !== 27 && topic !== 7) {
    if (topicMap[topic]) {
      topic500 = topicMap[topic];
    } else {
      topic500 = (
        await strapi.connections.default
          // .raw(
          //   `select id from posts where id <> ${id} and published_at is not null and topic = ${topic} order by viewNum desc limit 500`
          // )

          // posts with tag "friss" should also be excluded
          .raw(
            `select p.id from posts p left join posts_tags__tags_posts pt on p.id = pt.post_id and pt.tag_id <> 169 where p.id <> ${id} and p.published_at is not null and p.topic = ${topic} and p.featuredTag <> 169 order by p.viewNum desc limit 500`
          )
          .then()
      )[0];
      topicMap[topic] = topic500;
    }
  }

  let parsedSimilars = [];
  if (similarPosts) {
    parsedSimilars = Array.isArray(similarPosts)
      ? similarPosts
      : JSON.parse(similarPosts);
  }

  const usedIds = [id, ...parsedSimilars];
  const generatedIds = [];

  collectRandoms(usedIds, featured100, generatedIds, 6);
  collectRandoms(usedIds, topic500, generatedIds, 11);

  if (generatedIds.length) {
    await saveGenerateds(
      generatedIds.slice(0, 10 - parsedSimilars.length || 0),
      id
    );
  }
}

function collectRandoms(usedIds, collectFrom, collectTo, limit) {
  if (!collectFrom.length) {
    return;
  }
  let guard = collectFrom.length;
  let hits = 0;
  while (guard && hits < limit) {
    const rand = Math.round(Math.random() * (collectFrom.length - 1));
    if (!usedIds.includes(collectFrom[rand].id)) {
      collectTo.push(collectFrom[rand].id);
      usedIds.push(collectFrom[rand].id);
      hits++;
    }
    guard--;
  }
}

async function saveGenerateds(array = [], id) {
  if (array.length && id) {
    await strapi.connections.default
      .raw(
        `update posts set generatedSimilarPosts = '${JSON.stringify(
          array
        )}' where id = ${id}`
      )
      .then();
  }
}

module.exports = {
  generateAndSaveSimilars,
};
