"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

// percről - percre
const getLatest = async (ctx) => {
  const externals = await strapi.services["external-news"].find(
    {
      _limit: 5,
      _sort: "published_at:DESC",
      source_nin: ["vorosmarty_yt", "fmc_yt"],
    },
    []
  );

  const publishInfo = await strapi.query("publish-info").findOne({ id: 1 });

  const posts = await strapi.services.post.find(
    {
      _limit: 3,
      _sort: "firstPublishedAt:DESC",
      published_at_lte: publishInfo.lastPublished,
    },
    []
  );

  const result = [...externals, ...posts];
  result.sort(
    (a, b) =>
      new Date(b.published_at).getTime() - new Date(a.published_at).getTime()
  );

  return result.slice(0, 5);
};

// multimédia a frontpagen
const getVideos = async (ctx) => {
  const latestTv = await strapi.services["external-news"].findOne(
    {
      _limit: 1,
      _sort: "published_at:DESC",
      source: ["tv"],
    },
    []
  );
  const latestFMCVideo = await strapi.services["post"].find(
    {
      _limit: 3,
      _sort: "published_at:DESC",
      topic: 6, //multimedia
    },
    ["featuredImage", "featuredTag", "topic"]
  );
  /*const latestRadio = await strapi.services["external-news"].findOne(
    {
      _limit: 1,
      _sort: "published_at:DESC",
      source: ["vorosmarty_yt"],
      showOnMultimedia: true,
    },
    []
  );*/

  return [latestTv, ...latestFMCVideo /*, latestRadio*/];
};

module.exports = {
  getLatest,
  getVideos,
};
