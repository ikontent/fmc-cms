# FMC

## Install

- copy `.env.example` to `.env`
- `npm i`
- `docker-compose up -d`
- `npm run dev:admin`

## strapi commands

npm run dev:admin
Start Strapi in watch mode.

npm run start
Start Strapi without watch mode.

npm run build
Build Strapi admin panel.

npm run strapi
Display all available commands.

## mailhog

http://localhost:8025/
