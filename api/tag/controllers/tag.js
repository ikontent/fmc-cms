"use strict";
const axios = require("axios");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

let weatherCacheDate, weatherCached;
const WEATHERCHACHETIME = 1000 * 60 * 10; //ten minutes in milisec

module.exports = {
  async newsletterSubscribe(ctx) {
    const config = {
      method: "post",
      url: `https://${process.env.MAILCHIMP_SERVER}.${process.env.MAILCHIMP_URL}/lists/${process.env.MAILCHIMP_ID}/members`,
      data: ctx.request.body,
      auth: {
        username: "",
        password: process.env.MAILCHIMP_APIKEY,
      },
    };

    try {
      await axios(config);
    } catch (err) {
      console.log(err);
      if (err.response && err.response.data) {
        ctx.response.status = err.response.status;
        return err.response.data;
      }
    }

    return { success: true };
  },

  async weatherApi(ctx) {
    const config = {
      url: "https://api.weatherapi.com/v1/current.json",
      params: {
        key: process.env.WEATHERAPI_APIKEY,
        q: "Székesfehérvár",
      },
    };

    try {
      if (!weatherCached || Date.now() - weatherCacheDate > WEATHERCHACHETIME) {
        const response = await axios(config);
        if (response.data && response.data.current) {
          weatherCached = response.data;
          weatherCacheDate = Date.now();
        }
      }
      return weatherCached;
    } catch (err) {
      console.log(err.response.data);
      if (err.response && err.response.data) {
        ctx.response.status = err.response.status;
        return err.response.data;
      }
    }
  },

  async weatherStack(ctx) {
    const config = {
      url: "http://api.weatherstack.com/current",
      params: {
        access_key: process.env.WEATHERSTACK_APIKEY,
        query: "szekesfehervar",
      },
    };

    try {
      const response = await axios(config);

      return response.data;
    } catch (err) {
      console.log(err.response.data);
      if (err.response && err.response.data) {
        ctx.response.status = err.response.status;
        return err.response.data;
      }
    }
  },

  async ambeeWeather(ctx) {
    const config = {
      url: " https://api.ambeedata.com/weather/latest/by-lat-lng",
      headers: {
        "x-api-key": process.env.AMBEEWEATHER_APIKEY,
      },
      params: { lat: "47.1944491", lng: "18.3011689" },
    };

    try {
      const response = await axios(config);

      return response.data;
    } catch (err) {
      console.log(err.response.data);
      if (err.response && err.response.data) {
        ctx.response.status = err.response.status;
        return err.response.data;
      }
    }
  },

  async getLatestDrafts(ctx) {
    const knex = strapi.connections.default;
    const Post = () => knex("tags");
    const result = await Post()
      .select(["*"])
      .limit(20)
      .orderBy("created_at", "desc")
      .where({
        published_at: null,
      });

    return result || false;
  },
};
