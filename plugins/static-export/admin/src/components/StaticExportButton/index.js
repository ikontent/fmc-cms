import React, { memo, useEffect, useState } from "react";
import { auth, request } from "strapi-helper-plugin";
import styled from "styled-components";
import { Button as Base } from "@buffetjs/core";
import axios from "axios";

import moment from "moment";

let interval = null;

const StyledButton = styled(Base)`
  padding-left: 15px;
  padding-right: 15px;
`;

const StaticExportButton = () => {
  const [publishLoading, setPublishLoading] = useState(true);
  const [pipelineError, setPipelineError] = useState(false);
  const [lastSuccess, setLastSuccess] = useState();

  useEffect(() => {
    // pipeline poll when component mounted
    pollPipelineState();
  }, []);

  const pollPipelineState = async () => {
    try {
      const pipelineResponse = await request(
        `${STRAPI_URL}/static-export/check`,
        {
          method: "POST",
          body: {},
        }
      );

      if (pipelineResponse.error) {
        setPipelineError(true);
        return;
      }

      setPipelineError(false);
      const { pipelineState } = pipelineResponse;

      if (pipelineState.isCompleted) {
        clearInterval(interval);
        interval = null;
        setPublishLoading(false);

        if (!pipelineState.success) {
          setPipelineError(true);
          strapi.notification.error("A legutóbbi publikálás sikertelen volt!");
        }
        if (pipelineState.lastSuccess) {
          setLastSuccess(pipelineState.lastSuccess);
        }
      } else {
        setPublishLoading(true);
        if (!interval) {
          interval = setInterval(pollPipelineState, 5000);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const triggerPipeline = async () => {
    try {
      setPublishLoading(true);

      const pipelineResponse = await request(
        `${STRAPI_URL}/static-export/trigger`,
        {
          method: "POST",
          body: {},
        }
      );

      if (pipelineResponse.error) {
        setPipelineError(true);
        return;
      }

      setPipelineError(false);
      pollPipelineState();
      interval = setInterval(pollPipelineState, 5000);
    } catch (err) {
      setPublishLoading(false);
      setPipelineError(true);
    }
  };

  return (
    <div>
      <StyledButton
        label={pipelineError ? "EXPORT ERROR" : "EXPORT"}
        color={
          pipelineError ? "delete" : publishLoading ? "secondary" : "success"
        }
        onClick={triggerPipeline}
        isLoading={publishLoading}
        style={{
          padding: "4px 15px 2px",
          pointerEvents: publishLoading ? "none" : "auto",
        }}
      />
      {lastSuccess &&
        ` (Utolsó sikeres publikálás: ${moment(lastSuccess).format(
          "YYYY-MM-DD HH:mm"
        )})`}
    </div>
  );
};

export default memo(StaticExportButton);
