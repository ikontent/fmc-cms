"use strict";
const slugify = require("slugify");

/**
 * Lifecycle callbacks for the `User` model.
 */

module.exports = {
  lifecycles: {
    async beforeCreate(data) {
      if (data.name) {
        if (data.oldSlug && data.oldSlug != "") {
          data.slug = data.oldSlug;
        } else {
          data.slug = slugify(data.name, { lower: true, strict: true });
        }
      }
    },
    async beforeUpdate(params, data) {
      if (data.name) {
        if (data.oldSlug && data.oldSlug != "") {
          data.slug = data.oldSlug;
        } else {
          data.slug = slugify(data.name, { lower: true, strict: true });
        }
      }
    },
  },
};
