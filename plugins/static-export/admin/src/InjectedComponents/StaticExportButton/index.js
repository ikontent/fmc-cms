import React, { memo, useEffect, useState } from "react";
import { auth } from "strapi-helper-plugin";
import axios from "axios";

let interval = null;

const StaticExportButton = () => {
  const [publishLoading, setPublishLoading] = useState(true);
  const [pipelineError, setPipelineError] = useState(false);

  useEffect(() => {
    // pipeline poll when component mounted
    pollPipelineState();
  }, []);

  const pollPipelineState = async () => {
    try {
      const pipelineResponse = await axios({
        url: `${STRAPI_URL}/static-export/check/${BITBUCKET_PIPELINE_BRANCH}`,
        withCredentials: true,
        method: "POST",
        headers: {
          Authorization: `Bearer ${auth.getToken()}`,
        },
      });

      if (pipelineResponse.status === 500) {
        setPipelineError(true);
        return;
      }

      if (pipelineResponse.status === 200) {
        setPipelineError(false);
        const { pipelineState } = pipelineResponse.data;

        if (pipelineState.isCompleted) {
          clearInterval(interval);
          interval = null;
          setPublishLoading(false);

          if (!pipelineState.success) {
            setPipelineError(true);
            message.error("A legutóbbi publikálás sikertelen volt!");
          }
        } else {
          setPublishLoading(true);
          if (!interval) {
            interval = setInterval(pollPipelineState, 5000);
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const triggerPipeline = async () => {
    try {
      setPublishLoading(true);

      const pipelineResponse = await axios({
        url: `${STRAPI_URL}/static-export/trigger/${BITBUCKET_PIPELINE_BRANCH}`,
        withCredentials: true,
        method: "POST",
        headers: {
          Authorization: `Bearer ${auth.getToken()}`,
        },
      });

      if (pipelineResponse.status === 500) {
        setPipelineError(true);
        return;
      }

      if (pipelineResponse.status === 200) {
        setPipelineError(false);
        interval = setInterval(pollPipelineState, 5000);
      }
    } catch (err) {
      setPublishLoading(false);
      setPipelineError(true);
    }
  };

  return (
    <div className="p-4">
      <button
        color={
          pipelineError ? "delete" : publishLoading ? "secondary" : "success"
        }
        onClick={triggerPipeline}
        isLoading={publishLoading}
        style={{
          padding: "4px 15px 2px",
          pointerEvents: publishLoading ? "none" : "auto",
        }}
      >
        {pipelineError ? "PUBLISH ERROR" : "PUBLISH"}
      </button>
    </div>
  );
};

export default memo(StaticExportButton);
