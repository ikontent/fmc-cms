module.exports = {
  webpack: (config, webpack) => {
    // Add your variable using the DefinePlugin
    config.plugins.push(
      new webpack.DefinePlugin({
        BITBUCKET_PIPELINE_BRANCH: JSON.stringify(
          process.env.BITBUCKET_PIPELINE_BRANCH
        ),
        BITBUCKET_REPO: JSON.stringify(process.env.BITBUCKET_REPO),
        STRAPI_URL: JSON.stringify(process.env.STRAPI_URL),
        WEB_URL: JSON.stringify(process.env.WEB_URL),
        PREVIEW_API_URL_KEY: JSON.stringify(process.env.PREVIEW_API_URL_KEY),
        PREVIEW_CLIENT_URL_KEY: JSON.stringify(
          process.env.PREVIEW_CLIENT_URL_KEY
        ),
      })
    );
    return config;
  },
};
