"use strict";

/**
 * frontpage-posts.js controller
 *
 * @description: A set of functions called "actions" of the `frontpage-posts` plugin.
 */

module.exports = {
  /**
   * Default action.
   *
   * @return {Object}
   */

  index: async (ctx) => {
    // Add your own logic here.

    // Send 200 `ok`
    ctx.send({
      message: "ok",
    });
  },

  /**
   * Get the fronpage posts to the editor. The response contains the list state: isSaved (True if the live post list match the post list)
   */
  getPosts: async (ctx) => {
    try {
      const knex = strapi.connections.default;

      const FrontpageEditor = () => knex("front_page_editors");

      const {
        frontpagePosts: frontpagePosts,
        liveFrontpagePosts: liveFrontpagePosts,
      } = await FrontpageEditor()
        .select("frontpagePosts", "liveFrontpagePosts")
        .first();

      const publishInfo = await strapi.query("publish-info").findOne({ id: 1 });

      let result = {
        isSaved: frontpagePosts == liveFrontpagePosts,
        posts: [],
        lastPublished: publishInfo.lastPublished,
        editingUser: publishInfo.editingUser,
        editingTime: publishInfo.editingTime,
      };

      for (const [i, post] of JSON.parse(frontpagePosts).entries()) {
        const postData = await strapi
          .query("post")
          .findOne({ id: post.postId });
        result.posts.push({
          ...postData,
          isLocked: post.isLocked,
          frontPageDisplayStyle:
            post.frontPageDisplayStyle || (postData.showFeaturedImage ? 3 : 2),
          ...(post.removalDate && { removalDate: post.removalDate }),
        });
      }
      return result;
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },

  /**
   * Save the editor post list to frontpagePosts and liveFrontpagePosts
   */
  savePosts: async (ctx) => {
    try {
      const knex = strapi.connections.default;

      const requestData = JSON.stringify(ctx.request.body);
      const FrontpageEditor = () => knex("front_page_editors");
      await FrontpageEditor()
        .where({
          id: 1,
        })
        .update({
          frontpagePosts: requestData,
          liveFrontpagePosts: requestData,
        });

      return { result: "ok" };
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },

  /**
   * Copy posts to live list
   */
  copyPostsToLive: async (ctx) => {
    try {
      const knex = strapi.connections.default;

      const FrontpageEditor = () => knex("front_page_editors");
      const { frontpagePosts: frontpagePosts } = await FrontpageEditor()
        .select("frontpagePosts")
        .first();

      await FrontpageEditor()
        .where({
          id: 1,
        })
        .update({
          liveFrontpagePosts: frontpagePosts,
        });

      return { result: "ok" };
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },

  /**
   * Add an article to the list begining
   */
  addPost: async (ctx) => {
    try {
      const { id } = ctx.request.body;

      const knex = strapi.connections.default;

      const FrontpageEditor = () => knex("front_page_editors");
      const { frontpagePosts: frontpagePosts } = await FrontpageEditor()
        .select("frontpagePosts", "liveFrontpagePosts")
        .first();

      let posts = JSON.parse(frontpagePosts);
      if (posts.some((p) => p.postId == id)) {
        return { result: "error", msg: "The article has already been added" };
      }

      posts.splice(0, 0, {
        postId: id,
        isLocked: false,
      });

      for (let i = 1; i < posts.length; i++) {
        if (posts[i].isLocked) {
          const tmp = posts[i - 1];
          posts[i - 1] = posts[i];
          posts[i] = tmp;
        }
      }

      await FrontpageEditor()
        .where({
          id: 1,
        })
        .update({ frontpagePosts: JSON.stringify(posts) });

      return { result: "ok" };
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },

  /**
   * Check the post (by id) includes in the live frontpage. Returning a true or false value
   */
  hasFrontpageThisPost: async (ctx) => {
    const { id } = ctx.params;

    const knex = strapi.connections.default;
    const FrontpageEditor = () => knex("front_page_editors");
    const {
      liveFrontpagePosts: liveFrontpagePosts,
      frontpagePosts: frontpagePosts,
    } = await FrontpageEditor()
      .select("frontpagePosts", "liveFrontpagePosts")
      .first();

    const hasPosts = JSON.parse(frontpagePosts).some(
      (post) => post.postId == id
    );
    const hasLivePosts = JSON.parse(liveFrontpagePosts).some(
      (post) => post.postId == id
    );

    return hasPosts || hasLivePosts;
  },

  pingLock: async (ctx) => {
    try {
      const id = 1;
      const { id: userId } = ctx.state.user;
      const entity = await strapi.query("publish-info").findOne({ id });

      if (entity) {
        if (
          entity.editingUser &&
          entity.editingUser.id !== userId &&
          !isStuckedLock(entity.editingTime)
        ) {
          return { isOtherEditing: true };
        }

        await strapi.query("publish-info").update(
          { id },
          {
            editingUser: userId,
            editingTime: new Date(),
          }
        );
      } else {
        ctx.response.status = 404;
        return { error: "Nincs ilyen cikk!" };
      }

      return {
        success: true,
      };
    } catch (err) {
      console.log("ERROR AT LOCKING CIMLAPIPOSZTOPK", err);

      ctx.response.status = 500;
      return { error: "Hiba a zárásnál!" };
    }
  },

  pingUnLock: async (ctx) => {
    try {
      const id = 1;
      const { id: userId } = ctx.state.user;
      const entity = await strapi.query("publish-info").findOne({ id });
      if (entity) {
        if (
          entity.editingUser &&
          entity.editingUser.id !== userId &&
          !isStuckedLock(entity.editingTime)
        ) {
          return { isOtherEditing: true };
        }

        await strapi.query("publish-info").update(
          { id },
          {
            editingUser: null,
            editingTime: null,
          }
        );
      } else {
        ctx.response.status = 404;
        return { error: "Nincs ilyen cikk!" };
      }

      return {
        success: true,
      };
    } catch (err) {
      console.log("ERROR AT UNLOCKING CIMLAPIPOSZTOK", err);

      ctx.response.status = 500;
      return { error: "Hiba a visszanyitásnál!" };
    }
  },
  forceGetFromEditor: async (ctx) => {
    try {
      const id = 1;
      const { roles } = ctx.state.user;
      if (!roles.some((x) => x.id === 1)) {
        ctx.response.status = 401;
        return { error: true };
      }

      await strapi.query("publish-info").update(
        { id },
        {
          editingUser: null,
          editingTime: null,
        }
      );

      return {
        success: true,
      };
    } catch (err) {
      console.log("ERROR AT FORCEGET CIMLAPIPOSZTOK", err);

      ctx.response.status = 500;
      return { error: "Hiba a visszavevésnél!" };
    }
  },

  /**
   * Get the frontpage posts for the preview page
   */
  getFrontPagePreview: async (ctx) => {
    try {
      const newsBlockLengths = [9, 14, 19];

      const { previewKey } = ctx.params;

      if (previewKey != process.env.PREVIEW_API_URL_KEY) {
        ctx.response.status = 401;
        return;
      }

      const knex = strapi.connections.default;
      const FrontpageEditor = () => knex("front_page_editors");

      const { frontpagePosts: frontpagePostsData } = await FrontpageEditor()
        .select("frontpagePosts")
        .first();

      const frontpagePosts = JSON.parse(frontpagePostsData);
      const frontpagePostArray = frontpagePosts.map((p) => p.postId);

      const posts = await strapi
        .query("post")
        .find({ id_in: frontpagePostArray }, [
          "featuredImage",
          "featuredTag",
          "topic",
        ]);

      const postMap = posts.reduce((acc, post) => {
        acc[post.id] = post;
        return acc;
      }, {});

      const feedArticles = frontpagePosts
        .slice(
          0,
          newsBlockLengths[0] + newsBlockLengths[1] + newsBlockLengths[2]
        )
        .reduce((acc, curr) => {
          if (curr && postMap[curr.postId]) {
            acc.push({
              ...pick(
                postMap[curr.postId],
                "id",
                "featuredImage",
                "showFeaturedImage",
                "title",
                "lead",
                "published_at",
                "featuredTag",
                "topic",
                "isHighlightedContent",
                "isSponsoredContent",
                "sensitive",
                "firstPublishedAt",
                "created_at",
                "slug"
              ),
              frontPageDisplayStyle: curr.frontPageDisplayStyle,
            });
          }
          return acc;
        }, []);

      const news1 = feedArticles.slice(0, newsBlockLengths[0]);
      const news2 = feedArticles.slice(
        newsBlockLengths[0],
        newsBlockLengths[0] + newsBlockLengths[1]
      );
      const news3 = feedArticles.slice(
        newsBlockLengths[0] + newsBlockLengths[1],
        newsBlockLengths[0] + newsBlockLengths[1] + newsBlockLengths[2]
      );

      return {
        result: {
          news1,
          news2,
          news3,
        },
      };
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },

  /**
   * Save the editor post list to frontpagePosts for preview
   */
  savePreviewPosts: async (ctx) => {
    try {
      const knex = strapi.connections.default;
      const requestData = JSON.stringify(ctx.request.body);
      const FrontpageEditor = () => knex("front_page_editors");
      await FrontpageEditor()
        .where({
          id: 1,
        })
        .update({
          frontpagePosts: requestData,
        });

      return {
        result: "ok",
      };
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },
  /**
   * Get the frontpage featured tags
   */
  getMenuFeaturedTags: async (ctx) => {
    try {
      const knex = strapi.connections.default;
      const MenuFeaturedTags = () =>
        knex("components_front_page_editor_menu_featured_tags");
      const menuFeaturedTags = await MenuFeaturedTags();

      return {
        result: { tags: menuFeaturedTags },
      };
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },

  /**
   * Get the sidebar sponsored post
   */
  getSideBarSponsoredPost: async (ctx) => {
    try {
      const knex = strapi.connections.default;
      const FrontpageEditor = () => knex("front_page_editors");
      const { SidebarSponsoredPost: sidebarSponsoredPost } =
        await FrontpageEditor().first();

      return {
        result: {
          post: sidebarSponsoredPost
            ? await strapi.query("post").findOne({ id: sidebarSponsoredPost })
            : null,
        },
      };
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },

  getHighlightedBox: async (ctx) => {
    const FeaturedTags = strapi.db.query("front-page-editor.featured-tags");
    const tags = await FeaturedTags.find();
    const highlightedTags = await tags.map((tag) => {
      return {
        id: tag.id,
        blockName: tag.blockName,
        tags: tag.tags.map((t) => {
          return {
            value: t.id,
            label: t.tagName,
          };
        }),
      };
    });
    return {
      result: { tags: highlightedTags },
    };
  },

  getHighlightedTagsMostPopularArticles: async (ctx) => {
    const tags = ctx.request.body.map((t) => t.id);

    const MAX_POSTS = 3;

    const posts = (
      await strapi.connections.default
        .raw(
          `select * from (select p.id, p.title, p.published_at from posts p join posts_tags__tags_posts pt on p.id = pt.post_id and pt.tag_id in (${tags.join()}) and p.published_at is not null UNION ALL select id, title, published_at from posts where featuredTag in (${tags.join()}) order by published_at desc limit ${MAX_POSTS}) x order by x.published_at desc limit ${MAX_POSTS};`
        )
        .then()
    )[0];

    return {
      result: { posts },
    };
  },

  saveHighlightedBox: async (ctx) => {
    try {
      const FeaturedTags = strapi.query("front-page-editor.featured-tags");
      const { highlightedTags } = ctx.request.body;

      await Promise.all(
        highlightedTags.map(async (tag) => {
          const tags = tag.tags.map((t) => t.value);
          await FeaturedTags.update(
            { id: tag.id },
            {
              blockName: tag.blockName,
              tags,
            }
          );
        })
      );
      return { result: "ok" };
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },

  /**
   * Save the sidebar sponsored post
   */
  saveSponsoredPost: async (ctx) => {
    try {
      const knex = strapi.connections.default;
      const FrontpageEditor = () => knex("front_page_editors");
      const postId = ctx.request.body.postId || null;
      console.log(postId);
      await FrontpageEditor()
        .where({
          id: 1,
        })
        .update({
          SidebarSponsoredPost: postId,
        });

      return {
        result: "ok",
      };
    } catch (err) {
      console.log(err);
      return { result: "error", msg: err };
    }
  },
};

function isStuckedLock(lockDateString) {
  if (isNaN(new Date(lockDateString).valueOf())) {
    return true;
  }
  return (new Date() - new Date(lockDateString)) / 1000 > 30;
}

function pick(obj, ...props) {
  const result = {};
  for (const prop of props) {
    if (obj && obj.hasOwnProperty(prop)) {
      result[prop] = obj[prop];
    }
  }
  return result;
}
