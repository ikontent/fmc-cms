"use strict";
const { sanitizeEntity } = require("strapi-utils");
const RSS = require("rss-generator");
const OWN_URL = process.env.OWN_URL;
const WEB_URL = process.env.WEB_URL;

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const findByTag = async (ctx) => {
  const tagId = ctx.request.url.substring(ctx.request.url.lastIndexOf("/") + 1);
  const entities = await strapi.services.post.find({ tags: tagId });

  return entities.map((entity) =>
    sanitizeEntity(entity, { model: strapi.models.post })
  );
};

const findByTopic = async (ctx) => {
  const topicId = ctx.request.url.substring(
    ctx.request.url.lastIndexOf("/") + 1
  );
  const entities = await strapi.services.post.find({ tags: topicId });

  return entities.map((entity) =>
    sanitizeEntity(entity, { model: strapi.models.post })
  );
};

const incrementView = async (ctx) => {
  const { id } = ctx.params;
  const post = await strapi.services.post.findOne({ id });
  if (post) {
    await strapi.services.post.update(
      { id },
      {
        viewNum: post.viewNum ? parseInt(post.viewNum) + 1 : 1,
      }
    );
  }

  return {
    ok: true,
  };
};

const generateRSS = async (ctx) => {
  const posts = await strapi.services.post.find({
    _limit: 10,
    _sort: "published_at:DESC",
  });

  let feed = new RSS({
    title: "fmc.hu - Fehérvár összeköt",
    description:
      "Székesfehérvár új oldala: hírek, programok, érdekességek a városról és Fejér megyéről egyenesen a Fehérvár Médiacentrumtól. Jó helyen vagy!",
    feed_url: `${OWN_URL}/posts/rss`,
    site_url: WEB_URL,
    image_url: `${WEB_URL}/images/og-image.jpg`,
    language: "hu",
    pubDate: posts && posts.length ? posts[0].published_at : null,
  });

  for (const post of posts) {
    const publishedAt =
      post.firstPublishedAt || post.published_at || post.created_at;
    const date = publishedAt
      .substring(0, publishedAt.indexOf("T"))
      .replace(/-/g, "/");

    feed.item({
      title: post.title,
      description: post.lead,
      url: `${WEB_URL}/${date}/${post.slug}`,
      categories: post.topic ? [post.topic.name] : [], // optional - array of item categories
      author: post.author ? post.author.username : "", // optional - defaults to feed author property
      date: publishedAt, // any format that js Date can parse.
      enclosure: post.featuredImage
        ? {
            url: OWN_URL + post.featuredImage.url,
            size: Math.ceil(post.featuredImage.size * 1024),
            type: post.featuredImage.mime,
          }
        : null,
    });
  }

  return feed.xml();
};

const getPreview = async (ctx) => {
  const { previewKey, id } = ctx.params;

  if (previewKey != process.env.PREVIEW_API_URL_KEY) {
    ctx.response.status = 401;
    return;
  }

  const post = await strapi.query("post").findOne({ id });
  return post;
};

const getSimilar = async (ctx) => {
  if (ctx.query.id_nin) {
    const id_nin = ctx.query.id_nin.split(",");
    ctx.query.id_nin = id_nin;
  }
  if (ctx.query.id_in) {
    const id_in = ctx.query.id_in.split(",");
    ctx.query.id_in = id_in;
  }
  const posts = await strapi.services.post.find(ctx.query, []);

  return posts.map(({ id, title }) => ({
    id,
    title,
  }));
};

/**
 * Remove created_by and updated_by, cuz these fields contains the user password!
 * Sideeffect: convert bookshelf result object to plain object!
 */
const removeSensitiveData = (result) => {
  if (result) {
    result = result.toJSON();
    if (result.created_by) {
      result.created_by = null;
    }
    if (result.updated_by) {
      result.updated_by = null;
    }
  }

  return result;
};

const getRandom = async (ctx) => {
  const result = await strapi
    .query("post")
    .model.query((qb) => {
      qb.where("published_at", "!=", "null");
      qb.where("topic", "!=", 27); // Hírek topickból nem lehet válogatni
      qb.orderByRaw("RAND()");
      qb.limit(1);
    })
    .fetch();

  return removeSensitiveData(result) || false;
};

const getRandomByTopic = async (ctx) => {
  const { id } = ctx.params;
  if (id == 27) {
    return false;
  }

  let result = await strapi
    .query("post")
    .model.query((qb) => {
      qb.where("topic", id);
      qb.where("published_at", "!=", "null");
      qb.orderByRaw("RAND()");
      qb.limit(1);
    })
    .fetch();

  return removeSensitiveData(result) || false;
};

const getRandomByTag = async (ctx) => {
  const { id } = ctx.params;

  const result = await strapi
    .query("post")
    .model.query((qb) => {
      qb.where("topic", "!=", 27); // Hírek topickból nem lehet válogatni
      qb.where("featuredTag", id);
      qb.where("published_at", "!=", "null");
      qb.orderByRaw("RAND()");
      qb.limit(1);
    })
    .fetch();

  return removeSensitiveData(result) || false;
};

const getLatestDrafts = async (ctx) => {
  const { userId, isAuthor } = ctx.params;

  const knex = strapi.connections.default;
  const Post = () => knex("posts");
  const result = await Post()
    .select([
      "*",
      "posts.id as id",
      knex.raw("concat(admin.lastname, ' ', admin.firstname) as author"),
    ])
    .leftJoin("strapi_administrator as admin", function () {
      this.on("posts.author", "=", "admin.id");
    })
    .limit(20)
    .orderBy("created_at", "desc")
    .where({
      published_at: null,
    })
    .modify(function (queryBuilder) {
      if (isAuthor == 1) {
        queryBuilder.where("created_by", userId);
      }
    });

  return result || false;
};

const pingLock = async (ctx) => {
  try {
    const { id } = ctx.request.body;
    const { id: userId } = ctx.state.user;
    const post = await strapi.query("post").findOne({ id });

    if (post) {
      if (
        post.editingUser &&
        post.editingUser.id !== userId &&
        !isStuckedLock(post.editingTime)
      ) {
        return { isOtherEditing: true };
      }

      await strapi.services.post.update(
        { id },
        {
          editingUser: userId,
          editingTime: new Date(),
        }
      );
    } else {
      ctx.response.status = 404;
      return { error: "Nincs ilyen cikk!" };
    }

    return {
      success: true,
    };
  } catch (err) {
    console.log("ERROR AT LOCKING POST", err);

    ctx.response.status = 500;
    return { error: "Hiba a zárásnál!" };
  }
};

const pingUnLock = async (ctx) => {
  try {
    const { id } = ctx.request.body;
    const { id: userId } = ctx.state.user;
    const post = await strapi.query("post").findOne({ id });
    if (post) {
      if (
        post.editingUser &&
        post.editingUser.id !== userId &&
        !isStuckedLock(post.editingTime)
      ) {
        return { isOtherEditing: true };
      }

      await strapi.services.post.update(
        { id },
        {
          editingUser: null,
          editingTime: null,
        }
      );
    } else {
      ctx.response.status = 404;
      return { error: "Nincs ilyen cikk!" };
    }

    return {
      success: true,
    };
  } catch (err) {
    console.log("ERROR AT UNLOCKING POST", err);

    ctx.response.status = 500;
    return { error: "Hiba a visszanyitásnál!" };
  }
};
const forceGetFromEditor = async (ctx) => {
  try {
    const { id } = ctx.request.body;
    const { roles } = ctx.state.user;
    if (!roles.some((x) => x.id === 1)) {
      ctx.response.status = 401;
      return { error: true };
    }

    await strapi.services.post.update(
      { id },
      {
        editingUser: null,
        editingTime: null,
      }
    );

    return {
      success: true,
    };
  } catch (err) {
    console.log("ERROR AT FORCEGET POST", err);

    ctx.response.status = 500;
    return { error: "Hiba a visszavevésnél!" };
  }
};

module.exports = {
  getPreview,
  getRandom,
  getRandomByTopic,
  getRandomByTag,
  findByTag,
  findByTopic,
  incrementView,
  generateRSS,
  getSimilar,
  getLatestDrafts,
  pingLock,
  pingUnLock,
  forceGetFromEditor,
};

function isStuckedLock(lockDateString) {
  if (isNaN(new Date(lockDateString).valueOf())) {
    return true;
  }
  return (new Date() - new Date(lockDateString)) / 1000 > 30;
}
