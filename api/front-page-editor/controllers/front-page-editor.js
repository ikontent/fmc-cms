"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

function isStuckedLock(lockDateString) {
  if (isNaN(new Date(lockDateString).valueOf())) {
    return true;
  }
  return (new Date() - new Date(lockDateString)) / 1000 > 30;
}

const pingLock = async (ctx) => {
  try {
    const id = 1;
    const { id: userId } = ctx.state.user;
    const fpEditor = await strapi.query("front-page-editor").findOne({ id });

    if (fpEditor) {
      if (
        fpEditor.editingUser &&
        fpEditor.editingUser.id !== userId &&
        !isStuckedLock(fpEditor.editingTime)
      ) {
        return { isOtherEditing: true };
      }

      await strapi.query("front-page-editor").update(
        { id },
        {
          editingUser: userId,
          editingTime: new Date(),
        }
      );
    } else {
      ctx.response.status = 404;
      return { error: "Nincs ilyen cikk!" };
    }

    return {
      success: true,
    };
  } catch (err) {
    console.log("ERROR AT LOCKING Frontpage editor", err);

    ctx.response.status = 500;
    return { error: "Hiba a zárásnál!" };
  }
};

const pingUnLock = async (ctx) => {
  try {
    const id = 1;
    const { id: userId } = ctx.state.user;
    const fpEditor = await strapi.query("front-page-editor").findOne({ id });
    if (fpEditor) {
      if (
        fpEditor.editingUser &&
        fpEditor.editingUser.id !== userId &&
        !isStuckedLock(fpEditor.editingTime)
      ) {
        return { isOtherEditing: true };
      }

      await strapi.query("front-page-editor").update(
        { id },
        {
          editingUser: null,
          editingTime: null,
        }
      );
    } else {
      ctx.response.status = 404;
      return { error: "Nincs ilyen cikk!" };
    }

    return {
      success: true,
    };
  } catch (err) {
    console.log("ERROR AT UNLOCKING FRONTPAGE EDITOR", err);

    ctx.response.status = 500;
    return { error: "Hiba a visszanyitásnál!" };
  }
};

const forceGetFromEditor = async (ctx) => {
  try {
    const id = 1;
    const { roles } = ctx.state.user;
    if (!roles.some((x) => x.id === 1)) {
      ctx.response.status = 401;
      return { error: true };
    }

    await strapi.query("front-page-editor").update(
      { id },
      {
        editingUser: null,
        editingTime: null,
      }
    );

    return {
      success: true,
    };
  } catch (err) {
    console.log("ERROR AT FORCEGET FRONTPAGE EDITOR", err);

    ctx.response.status = 500;
    return { error: "Hiba a visszavevésnél!" };
  }
};

module.exports = { pingLock, pingUnLock, forceGetFromEditor };
