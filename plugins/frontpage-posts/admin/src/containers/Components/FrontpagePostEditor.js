import React, { memo, useEffect, useState, useRef } from "react";
import { ReactSortable } from "react-sortablejs";
import useDebounce from "../../utils/useDebounce";
import { request, LoadingIndicator, auth } from "strapi-helper-plugin";
import moment from "moment";
import { DateTime } from "@buffetjs/custom";
import { SponsoredContent } from "./SponsoredContent";
import { HighlightedBox } from "./HighlightedBoxes";
import { ClickoutsideWrapper } from "./ClickoutsideWrapper";

import { Link } from "react-router-dom";

let isLockOkay, interval;
function getIslockOkay() {
  return isLockOkay;
}

import { unstable_batchedUpdates } from "react-dom";

import "./FrontpagePostEditor.css";

const FrontpagePostEditor = () => {
  const [isSaved, setIsSaved] = useState(true);
  const [isLoaded, setIsLoaded] = useState();
  const [posts, setPosts] = useState([]);
  const [search, setSearch] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [searchLoading, setSearchLoading] = useState(false);
  const [completedOn, setCompletedOn] = useState(null); //last completed pipeline time in unix
  const debouncedSearch = useDebounce(search, 500);
  const [lockData, setLockData] = useState();
  const [areWeIn, setAreWeIn] = useState();
  const [dateEdit, setDateEdit] = useState();
  const [frontpageTags, setFrontpageTags] = useState([]);

  const [removalDate, setRemovalDate] = useState("");

  const highlightedTagsRef = useRef();
  const sponsoredContentRef = useRef();

  const newsBlockLengths = [9, 14, 19];

  useEffect(() => {
    (async () => {
      try {
        const frontpagePost = await request(
          `${strapi.backendURL}/frontpage-posts/get-posts`
        );
        const frontpageFeaturedTags = await request(
          `${strapi.backendURL}/frontpage-posts/featured-menu-tags`
        );

        isLockOkay = await pingLock();

        unstable_batchedUpdates(() => {
          setIsSaved(frontpagePost.isSaved);
          setPosts(frontpagePost.posts);
          setCompletedOn(new Date(frontpagePost.lastPublished).getTime());
          setIsLoaded(true);
          setLockData({
            editingTime: frontpagePost.editingTime,
            editingUser: frontpagePost.editingUser,
          });
          setFrontpageTags(frontpageFeaturedTags.result.tags);
          if (isLockOkay) {
            interval = setInterval(() => pingLock(true), 10000);
            setAreWeIn(true);
          }
        });
      } catch (err) {
        console.log(err);
        strapi.notification.error("Something happened during loading posts!");
      }
    })();
    return () => {
      if (getIslockOkay()) {
        clearInterval(interval);
        pingUnLock("posts");
      }
    };
  }, []);

  useEffect(() => {
    (async () => {
      try {
        if (debouncedSearch == "") {
          unstable_batchedUpdates(() => {
            setSearchLoading(false);
            setSearchResults([]);
          });
        } else {
          setSearchLoading(true);
          const results = await request(
            `${strapi.backendURL}/posts?title_contains=${debouncedSearch}&_sort=published_at:DESC&_limit=10`
          );
          unstable_batchedUpdates(() => {
            setSearchResults(results);
            setSearchLoading(false);
          });
        }
      } catch (err) {
        console.log(err);
        strapi.notification.error("Something happened during loading posts!");
      }
    })();
  }, [debouncedSearch]);

  useEffect(() => {
    const maxPost =
      newsBlockLengths[0] + newsBlockLengths[1] + newsBlockLengths[2] + 10;

    if (posts.length > maxPost) {
      posts.splice(maxPost - 1);
      setPosts(posts);
    }
  }, [posts]);

  const toggleLock = (post) => {
    const index = posts.findIndex((p) => p.id == post.id);
    if (index !== -1) {
      posts[index].isLocked = !posts[index].isLocked;
      setPosts([...posts]);
    }
  };

  const saveRemovalDate = (removalDate, post) => {
    if (removalDate === "") alert("Meg kell adnod dátumot");

    const index = posts.findIndex((p) => p.id == post.id);
    if (index !== -1) {
      posts[index].removalDate = removalDate;
      setPosts([...posts]);
    }
  };

  const deleteRemovalDate = (post) => {
    const index = posts.findIndex((p) => p.id == post.id);
    if (index !== -1) {
      delete posts[index].removalDate;
      setPosts([...posts]);
    }
  };

  const setFrontPageDisplayStyle = (post, displayStyle) => {
    const index = posts.findIndex((p) => p.id == post.id);
    if (index !== -1) {
      posts[index].frontPageDisplayStyle = displayStyle;
      setPosts([...posts]);
    }
  };

  const setSponsoredContent = async (post) => {
    const result = await request(`${strapi.backendURL}/posts/${post.id}`, {
      method: "PUT",
      body: {
        isSponsoredContent: !post.isSponsoredContent,
      },
    });

    const index = posts.findIndex((p) => p.id == post.id);
    if (index !== -1) {
      posts[index].isSponsoredContent = !post.isSponsoredContent;
      setPosts([...posts]);
    }
  };

  const highlightContent = async (post) => {
    const result = await request(`${strapi.backendURL}/posts/${post.id}`, {
      method: "PUT",
      body: {
        isHighlightedContent: !post.isHighlightedContent,
      },
    });
    const index = posts.findIndex((p) => p.id == post.id);
    if (index !== -1) {
      posts[index].isHighlightedContent = !post.isHighlightedContent;
      setPosts([...posts]);
    }
  };

  const removePost = (post) => {
    if (
      posts.length <=
      newsBlockLengths[0] + newsBlockLengths[1] + newsBlockLengths[2]
    ) {
      alert("Nem lehet törölni több cikket!");
      return;
    }
    if (
      confirm(
        `Biztos hogy törölni akarja a listáról a \n${post.title}\n cikket?`
      )
    ) {
      const index = posts.findIndex((p) => p.id == post.id);

      if (index === -1) {
        return;
      }

      if (posts.map((p) => p.isLocked).lastIndexOf(true) + 1 >= posts.length) {
        alert("Nem lehet több elemet törölni");
        return;
      }

      let newPosts = [...posts];
      newPosts.splice(index, 1);

      for (let i = newPosts.length - 1; i >= index; i--) {
        if (newPosts[i].isLocked) {
          const tmp = newPosts[i + 1];
          newPosts[i + 1] = newPosts[i];
          newPosts[i] = tmp;
        }
      }

      setPosts(newPosts);
    }
  };

  const saveFrontPageEditor = async () => {
    const validBox = await highlightedTagsRef.current.saveHighlightedBoxes();
    if (!validBox) return;
    await sponsoredContentRef.current.saveSaveSponsoredContent();
    await save();
  };
  const addPost = (post) => {
    if (posts.some((p) => p.id == post.id)) {
      alert("A cikk már hozzá van adva a címlaphoz!");
      return;
    }
    setSearchResults([]);
    setSearch("");

    let newPosts = [...posts];

    newPosts.splice(0, 0, {
      ...post,
      isLocked: false,
    });

    for (let i = 1; i < posts.length; i++) {
      if (newPosts[i].isLocked) {
        const tmp = newPosts[i - 1];
        newPosts[i - 1] = newPosts[i];
        newPosts[i] = tmp;
      }
    }
    setPosts(newPosts);
  };

  const savePreviewPosts = async () => {
    const result = await request(
      `${strapi.backendURL}/frontpage-posts/save-preview-posts`,
      {
        method: "POST",
        body: posts.map((p, i) => {
          return {
            postId: p.id,
            isLocked: p.isLocked,
            ...(i < 9 && { frontPageDisplayStyle: p.frontPageDisplayStyle }),
            ...(p.removalDate && { removalDate: p.removalDate }),
          };
        }),
      }
    );
    if (result?.result === "ok") {
      setIsSaved(false);
      strapi.notification.success("Előnézet sikeresen mentve");
      window.open(
        `${WEB_URL}/preview-frontpage/${PREVIEW_CLIENT_URL_KEY}?previewApiKey=${PREVIEW_API_URL_KEY}`
      );
    }
  };

  const save = async () => {
    const result = await request(
      `${strapi.backendURL}/frontpage-posts/save-posts`,
      {
        method: "POST",
        body: posts.map((p, i) => {
          return {
            postId: p.id,
            isLocked: p.isLocked,
            ...(i < 9 && { frontPageDisplayStyle: p.frontPageDisplayStyle }),
            ...(p.removalDate && { removalDate: p.removalDate }),
          };
        }),
      }
    );

    if (result?.result === "ok") {
      setIsSaved(true);
      strapi.notification.success("Sikeresen mentve");

      await request(`${STRAPI_URL}/static-export/trigger`, {
        method: "POST",
      });
    }
  };

  return isLoaded ? (
    !areWeIn && isEditingByOthers(lockData, auth) ? (
      <RenderOtherEditing
        editingUser={lockData.editingUser}
        editingTime={lockData.editingTime}
        auth={auth}
      />
    ) : (
      <div className="fpe" style={{ padding: "1rem" }}>
        <div
          style={{
            display: "flex",
            gap: "2rem",
          }}
        >
          <h1 style={{ flex: 1 }}>FMC CÍMLAPSZERKESZTŐ</h1>

          <Link
            to={`/plugins/content-manager/singleType/application::front-page-editor.front-page-editor`}
            style={{
              background: "#44f",
              padding: "1rem 1.5rem",
              color: "#fff",
              borderRadius: "4px",
              textDecoration: "none",
            }}
          >
            <a type="button">Breaking News</a>
          </Link>

          <button
            style={{
              background: "#44f",
              padding: "1rem 1.5rem",
              color: "#fff",
              borderRadius: "4px",
            }}
            onClick={() => {
              savePreviewPosts();
            }}
          >
            Címlap előnézet
          </button>

          <button
            style={{
              background: "#44f",
              padding: "1rem 1.5rem",
              color: "#fff",
              borderRadius: "4px",
            }}
            onClick={saveFrontPageEditor}
          >
            Mentés
          </button>
        </div>

        {completedOn &&
          `Legutóbbi exportálás ideje: ${moment(completedOn).format(
            "YYYY.MM.DD HH:mm"
          )}`}
        {!isSaved && (
          <p
            style={{
              padding: "1rem",
              background: "#ffb600",
              border: "1px solid red",
            }}
          >
            A változtatások még nincsenek mentve így exportáláskor nem kerül ki
            az éles oldalra
          </p>
        )}

        <SponsoredContent ref={sponsoredContentRef} />
        <div
          style={{
            margin: "4rem 0",
            display: "flex",
          }}
        >
          <h2>Kiemelt címkék</h2>
          {frontpageTags.length > 0 &&
            frontpageTags.map((tag) => (
              <div
                style={{
                  marginLeft: "2rem",
                }}
                key={tag.id}
              >
                {tag.label}
              </div>
            ))}
          <Link
            to={`/plugins/content-manager/singleType/application::front-page-editor.front-page-editor`}
            style={{
              color: "blue",
              textDecoration: "underline",
              marginLeft: "2rem",
            }}
          >
            Szerkesztés
          </Link>
        </div>
        <HighlightedBox ref={highlightedTagsRef} />

        <div
          style={{
            margin: "4rem 0",
          }}
        >
          <h2>Hírfolyam</h2>
        </div>
        <h3>Keresés a hír - archívumban</h3>
        <div className="row">
          <div className="col-md-6 col-12">
            <input
              type="text"
              style={{
                background: "#fff",
                padding: "0.5rem",
                border: "1px solid #777",
                marginBottom: "2rem",
              }}
              value={search}
              onChange={(e) => {
                setSearch(e.target.value);
              }}
            />
          </div>
        </div>
        {searchResults.length > 0 && (
          <>
            <h5>Találatok:</h5>
            <div className="search-result">
              {searchResults.map((post) => (
                <div
                  key={post.id}
                  onClick={() => addPost(post)}
                  style={{
                    fontWeight:
                      new Date(
                        post?.publishDate || post?.published_at
                      ).getTime() > completedOn
                        ? "bold"
                        : "normal",
                  }}
                >
                  {post.title} (
                  {moment(post?.publishDate || post?.published_at).format(
                    "yyyy.MM.DD"
                  )}
                  )
                </div>
              ))}
            </div>
          </>
        )}
        {searchLoading && (
          <div className="lds-ring">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        )}
        {posts.length ? (
          <ReactSortable
            list={posts}
            setList={() => {}}
            animation={200}
            filter=".locked"
            forceFallback={true}
            swap={true}
            disabled={dateEdit > 3}
            swapClass="highlight"
            invertSwap={true}
            onMove={(evt, originalEvent) => {
              if (evt.related) {
                return !evt.related.classList.contains("locked");
              }
            }}
            onEnd={(evt) => {
              let oldI = evt.oldIndex;
              let newI = evt.newIndex;

              if (
                oldI == newI ||
                posts[oldI].isLocked ||
                posts[newI].isLocked
              ) {
                return;
              }

              let newPosts = [...posts];

              let element = newPosts[oldI];
              newPosts.splice(oldI, 1);
              newPosts.splice(newI, 0, element);

              if (oldI > newI) {
                for (let i = newI; i <= oldI; i++) {
                  if (newPosts[i].isLocked) {
                    const tmp = newPosts[i - 1];
                    newPosts[i - 1] = newPosts[i];
                    newPosts[i] = tmp;
                  }
                }
              } else if (oldI < newI) {
                for (let i = newI; i >= oldI; i--) {
                  if (newPosts[i].isLocked) {
                    const tmp = newPosts[i + 1];
                    newPosts[i + 1] = newPosts[i];
                    newPosts[i] = tmp;
                  }
                }
              }
              setPosts(newPosts);
            }}
          >
            {(posts || []).map((post, i) => (
              <React.Fragment key={`post-${post.id}`}>
                <div
                  data-id={post.id}
                  style={{
                    display: "flex",
                    gap: "10px",
                    margin: "0.5rem 0",
                    alignItems: "center",
                  }}
                  className={`sortable-item ${post.isLocked ? "locked" : ""} ${
                    newsBlockLengths[0] +
                      newsBlockLengths[1] +
                      newsBlockLengths[2] <=
                    i
                      ? "not-showed"
                      : ""
                  } ${
                    i == newsBlockLengths[0] ||
                    i == newsBlockLengths[0] + newsBlockLengths[1] ||
                    i ==
                      newsBlockLengths[0] +
                        newsBlockLengths[1] +
                        newsBlockLengths[2]
                      ? "separator"
                      : ""
                  }
              `}
                >
                  <strong
                    style={{
                      cursor: post.isLocked ? "default" : "grab",
                      color: post.isLocked ? "red" : "inherit",
                    }}
                  >
                    {i + 1}.
                  </strong>{" "}
                  <section
                    style={{
                      display: "flex",
                      gap: "10px",
                      width: "100%",
                      justifyContent: "space-between",
                      background: "#e0e0e0",
                      padding: "0.5rem 2rem",
                      borderRadius: "5px",
                      fontWeight:
                        new Date(
                          post?.publishDate || post?.published_at
                        ).getTime() > completedOn
                          ? "bold"
                          : "normal",
                    }}
                  >
                    {i <= newsBlockLengths[0] - 1 && (
                      <select
                        style={{ background: "white", borderRadius: ".2rem" }}
                        value={Number(post.frontPageDisplayStyle)}
                        onChange={({ target }) => {
                          setFrontPageDisplayStyle(post, target.value);
                        }}
                      >
                        <option value={3}>Cím + lead + kép</option>
                        <option value={2}>Cím + lead</option>
                        <option value={1}>Cím + kép</option>
                        <option value={0}>Cím</option>
                      </select>
                    )}
                    <div
                      style={{
                        cursor: post.isLocked ? "default" : "grab",
                        width: "100%",
                      }}
                    >
                      {post.title} (
                      {moment(post?.publishDate || post?.published_at).format(
                        "yyyy.MM.DD"
                      )}
                      )
                    </div>

                    <div style={{ display: "flex", position: "relative" }}>
                      <div
                        style={{
                          whiteSpace: "nowrap",
                        }}
                      >
                        {post.removalDate &&
                          moment(post.removalDate).format("YYYY-MM-DD HH:mm")}
                      </div>
                      <button
                        style={{
                          filter: post.removalDate ? "none" : "grayscale(1)",
                          marginRight: "1rem",
                        }}
                        onClick={() => {
                          setDateEdit(i);
                          setRemovalDate(post.removalDate || new Date());
                        }}
                      >
                        ⏰
                      </button>
                      {dateEdit === i && (
                        <ClickoutsideWrapper
                          handler={() => setDateEdit(undefined)}
                        >
                          <div
                            style={{
                              position: "absolute",
                              background: "white",
                              borderRadius: "2rem",
                              padding: "2rem",
                              marginTop: "2rem",
                              zIndex: 99,
                              left: -200,
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                              }}
                            >
                              <h3
                                style={{
                                  margin: "1rem 0",
                                }}
                              >
                                Válassz dátumot és időpontot!
                              </h3>
                              <DateTime
                                name="datetime"
                                onChange={({ target: { value } }) => {
                                  setRemovalDate(value);
                                }}
                                value={removalDate}
                              />
                            </div>
                            <div
                              style={{
                                display: "flex",
                                marginTop: "2rem",
                              }}
                            >
                              <button
                                style={{
                                  background: "#fff",
                                  padding: "1rem 1.5rem",
                                  color: "#44f",
                                  border: "2px solid #44f",
                                  borderRadius: "4px",
                                  marginRight: "2rem",
                                }}
                                onClick={() => {
                                  setDateEdit(undefined);
                                }}
                              >
                                Mégsem
                              </button>

                              <button
                                style={{
                                  background: "red",
                                  padding: "1rem 1.5rem",
                                  color: "white",
                                  borderRadius: "4px",
                                  marginRight: "3rem",
                                }}
                                onClick={() => {
                                  deleteRemovalDate(post);
                                  setDateEdit(undefined);
                                }}
                              >
                                Törlés
                              </button>
                              <button
                                style={{
                                  background: "#44f",
                                  padding: "1rem 1.5rem",
                                  color: "#fff",
                                  borderRadius: "4px",
                                  marginRight: "3rem",
                                }}
                                onClick={() => {
                                  saveRemovalDate(removalDate, post);
                                  setDateEdit(undefined);
                                }}
                              >
                                Mentés
                              </button>
                            </div>
                          </div>
                        </ClickoutsideWrapper>
                      )}
                      <button
                        onClick={() => setSponsoredContent(post)}
                        style={{
                          marginRight: "1rem",
                          filter: post.isSponsoredContent
                            ? "none"
                            : "grayscale(1)",
                        }}
                      >
                        🟧
                      </button>
                      <button
                        onClick={() => highlightContent(post)}
                        style={{
                          marginRight: "1rem",
                          filter: post.isHighlightedContent
                            ? "none"
                            : "grayscale(1)",
                        }}
                      >
                        ⭐️
                      </button>

                      <button
                        style={{ marginRight: "2rem" }}
                        onClick={() => {
                          removePost(post);
                        }}
                      >
                        ✖
                      </button>
                      <Link
                        to={`/plugins/content-manager/collectionType/application::post.post/${post.id}`}
                        style={{
                          color: "#292b2c",
                          textDecoration: "none",
                          marginRight: "2rem",
                        }}
                      >
                        ✎
                      </Link>
                      <button
                        style={{
                          filter: post.isLocked ? "none" : "grayscale(1)",
                        }}
                        onClick={() => {
                          toggleLock(post);
                        }}
                      >
                        🔒
                      </button>
                    </div>
                  </section>
                </div>
              </React.Fragment>
            ))}
          </ReactSortable>
        ) : (
          <div className="lds-ring">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        )}
      </div>
    )
  ) : (
    <LoadingIndicator />
  );
};

export default memo(FrontpagePostEditor);

function isEditingByOthers(data = {}, auth) {
  return data.editingUser && data.editingUser.id !== auth.getUserInfo().id;
}

async function forceGetFromEditor() {
  try {
    const response = await request(
      `${strapi.backendURL}/frontpage-posts/forceget`,
      {
        method: "POST",
        body: {},
      }
    );
    if (response.error) {
      return strapi.notification.error(
        "Hiba történt a szerkesztőtől elvevés közben!"
      );
    }
    location.reload();
  } catch (err) {
    strapi.notification.error("Hiba történt a szerkesztőtől elvevés közben!");
  }
}

async function pingLock(isPolling) {
  try {
    const response = await request(
      `${strapi.backendURL}/frontpage-posts/lock`,
      {
        method: "POST",
        body: {},
      }
    );
    if (response.error) {
      strapi.notification.error("Hiba történt a szerkesztés lezárásánál!");
      return false;
    }
    if (isPolling && response.isOtherEditing) {
      location.reload();
    }

    return !response.isOtherEditing;
  } catch (err) {
    strapi.notification.error("Hiba történt a szerkesztés lezárásánál!");
    return false;
  }
}

async function pingUnLock() {
  try {
    const response = await request(
      `${strapi.backendURL}/frontpage-posts/unlock`,
      {
        method: "POST",
        body: {},
      }
    );
    if (response.error) {
      strapi.notification.error("Hiba történt a szerkesztés feloldása közben!");
    }
  } catch (err) {
    strapi.notification.error("Hiba történt a szerkesztés feloldása közben!");
  }
}

function RenderOtherEditing({ editingUser, editingTime, auth }) {
  return (
    <div style={{ marginTop: "40px", marginLeft: "24px" }}>
      <span
        style={{ color: "red", fontSize: "18px" }}
      >{`Sajnos most ezt ${editingUser.lastname} ${editingUser.firstname} szerkeszti!`}</span>
      <br />
      {editingTime && (
        <span>{`(Utolsó szerkesztési pillanat: ${moment(editingTime).format(
          "YYYY-MM-DD HH:mm:ss"
        )})`}</span>
      )}
      {/* {auth.getUserInfo().roles.some((x) => x.id === 1) && (
        <div
          style={{
            cursor: "pointer",
            border: "1px solid black",
            width: "120px",
          }}
          onClick={() => {
            forceGetFromEditor();
          }}
        >
          {" "}
          Elveszem a szerkesztést
        </div>
      )} */}
    </div>
  );
}
