import React from "react";

import useDataManager from "../../hooks/useDataManager";

import { ReactSortable } from "react-sortablejs";

const EditAnswers = () => {
  const { initialData, onChange, modifiedData } = useDataManager();

  let { answers } = modifiedData;

  if (!answers) {
    answers = [];
  }

  const isChanged = (orig, dest) => {
    return (
      !orig || dest.some(({ text }, index) => text !== orig?.[index]?.text)
    );
  };
  return answers && answers.map ? (
    <div className="row" style={{ paddingBottom: "3rem" }}>
      <div className="col-6">
        <h1>Answers</h1>

        <ReactSortable
          list={answers}
          setList={() => {}}
          animation={200}
          forceFallback={true}
          onEnd={(evt) => {
            let oldI = evt.oldIndex;
            let newI = evt.newIndex;

            let newAnswers = [...answers];

            let element = newAnswers[oldI];
            newAnswers.splice(oldI, 1);
            newAnswers.splice(newI, 0, element);

            onChange({
              target: {
                value: isChanged(initialData.answers, newAnswers)
                  ? [...newAnswers]
                  : [...initialData.answers],
                name: "answers",
                type: "json",
              },
            });
          }}
        >
          {answers.map(({ text, hash }, index) => (
            <div className="d-flex answer-editor" key={hash + index}>
              <input
                defaultValue={text}
                onChange={(e) => {
                  answers[index].text = e.target.value;
                  onChange({
                    target: {
                      value: isChanged(initialData.answers, answers)
                        ? [...answers]
                        : [...initialData.answers],
                      name: "answers",
                      type: "json",
                    },
                  });
                }}
                className="iuhLQs"
                style={{
                  margin: "2rem 0",
                  display: "block",
                  width: "100%",
                  height: "3.4rem",
                  padding: "0 1rem",
                  border: "1px solid #E3E9F3",
                  borderRadius: "2px",
                }}
              />
              <button
                onClick={(e) => {
                  e.stopPropagation();
                  e.preventDefault();
                  onChange({
                    target: {
                      value: answers.filter((_, i) => i !== index),
                      name: "answers",
                      type: "json",
                    },
                  });
                }}
              >
                ❌
              </button>
            </div>
          ))}
        </ReactSortable>
        <button
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            onChange({
              target: {
                value: [
                  ...answers,
                  { text: "", count: 0, hash: generateHash() },
                ],
                name: "answers",
                type: "json",
              },
            });
          }}
          style={{
            backgroundColor: "#E3E9F3",
            margin: "2rem 0",
            display: "block",
            width: "100%",
            height: "3.4rem",
            padding: "0 1rem",
            border: "1px solid gainsboro",
            borderRadius: "2px",
          }}
        >
          Válasz hozzáadása
        </button>
      </div>
    </div>
  ) : null;
};
export default EditAnswers;

const generateHash = () =>
  [1, 2, 3, 4, 5, 6].reduce(
    (acc, curr) => acc + String.fromCharCode(Math.random() * 100),
    ""
  );
