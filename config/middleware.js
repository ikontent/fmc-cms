module.exports = {
  load: {
    before: ["responseTime", "logger", "cors", "responses"],
    order: ["parser"],
    after: ["router"],
  },
  settings: {
    parser: {
      enabled: true,
      multipart: true,
    },
    cors: {
      enabled: true,
      origin: [
        "*",
        //process.env.WEB_URL,
        //process.env.LANDING_URL,
        //"http://localhost:8000",
        //"http://localhost:3000",
        //"http://localhost:1337",
      ],
    },
  },
};
